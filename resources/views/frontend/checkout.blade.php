@extends('layouts.cursos.app')
@section('content')
    <div class="row">
                    <!-- Start col -->
                    <div class="col-lg-12">
                        <div class="card m-b-30">
                            <div class="card-header">
                                <h5 class="card-title">{{ $title}}</h5>
                            </div>
                            <div class="card-body">
                                <div class="row justify-content-center">
                                    <div class="col-md-8">
                                        <div id="">
                                            <front-checkout-component @auth
                                
                                            :auth='{{Auth::user()->id}}' @else :auth="false" @endauth></front-checkout-component>
                                        </div>  
                                    </div>  
                                </div>                             
                            </div>
                        </div>
                    </div>
                    <!-- End col -->
                </div>
@endsection