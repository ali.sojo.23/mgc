<!-- Start Leftbar -->
<div class="leftbar">
    <!-- Start Sidebar -->
    <div class="sidebar">
        <!-- Start Logobar -->
        @include('layouts.dashboard.assets.leftbar.logobar')
        <!-- End Logobar -->
        <!-- Start Profilebar -->
        @include('layouts.dashboard.assets.leftbar.profilebar')
        <!-- End Profilebar -->
        <!-- Start Navigationbar -->
        @include('layouts.dashboard.assets.leftbar.navigationbar')
        <!-- End Navigationbar -->
    </div>
    <!-- End Sidebar -->
</div>
<!-- End Leftbar -->