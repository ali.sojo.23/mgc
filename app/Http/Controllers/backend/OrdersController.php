<?php

namespace App\Http\Controllers\backend;

use App\CursosOrders;
use App\Http\Controllers\Controller;
use App\orders;
use Illuminate\Http\Request;

class OrdersController extends Controller
{
    public function __construct() {
        $this->middleware('auth:sanctum');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if (!is_null($request->status) && !$request->user) :
            $orders = orders::where('status','LIKE', $request->status)->with(['items', 'users']);
        elseif (is_null($request->status) && $request->user) :
            $orders = orders::where('user_id', $request->user)->with(['items', 'users']);
        elseif (!is_null($request->status) && $request->user) :
            $orders = orders::where([['status', $request->status], ['user_id', $request->user]])->with(['items', 'users']);
        else :
            $orders = orders::with(['items', 'users']);
        endif;
        return $orders->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $user = $request->user;
        $orders = orders::create([
            'user_id' => $user,
            'price' => $request->total,
            'promo_codes_id' => $request->cupon,
        ]);
        foreach ($request->cars as $item => $value) {

            CursosOrders::create([
                'curso_id' => $value['id'],
                'order_id' => $orders->id
            ]);
        };

        return $orders;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\orders  $orders
     * @return \Illuminate\Http\Response
     */
    public function show(orders $orders)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\orders  $orders
     * @return \Illuminate\Http\Response
     */
    public function edit(orders $orders)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\orders  $orders
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, orders $orders)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\orders  $orders
     * @return \Illuminate\Http\Response
     */
    public function destroy(orders $order)
    {
        $order->delete();
        return $order;
    }
}
