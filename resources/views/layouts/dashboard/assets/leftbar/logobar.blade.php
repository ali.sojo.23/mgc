<div class="logobar">
    <a href="/dashboard" class="logo logo-large"><img src="{{asset('assets\images\logo.svg')}}" class="img-fluid" alt="logo"></a>
    <a href="/dashboard" class="logo logo-small"><img src="{{asset('assets\images\small_logo.svg')}}" class="img-fluid" alt="logo"></a>
</div>