@extends('layouts.auth.app')

@section('content')
<style>
    .auth-box.login-box .auth-box-right .card {
        padding-bottom: 116px 
    }
    @media (max-width: 767px){
        .auth-box.login-box .auth-box-right .card {
        padding-bottom: 0px 
    }
    }
</style>
<div class="card-body">
    <form method="POST" action="{{ route('password.update') }}">
        @csrf
        <input type="hidden" name="token" value="{{ $token }}">
        <h4 class="text-primary mb-4">Reset Password ?</h4>
        <p class="mb-4">Rellene los siguientes campos para completar el reinicio de la contraseña.</p>
        <div class="form-group">
            <input type="email" placeholder="Enter Email here" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>

            @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
        <div class="form-group">
            <input type="password" placeholder="Enter Passwor here" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

            @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror

        </div>
        <div class="form-group">
            <input id="password-confirm" placeholder="Re-Type Password here" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
        </div>                         
      <button type="submit" class="btn btn-success btn-lg btn-block font-18">Reset Password</button>
    </form>
</div>
@endsection
