<div class="profilebar text-center">
    <img src="{{asset('assets/images/users/boy.svg')}}" class="img-fluid" alt="profile">
    <div class="profilename">
      <h5>{{Auth::user()->firstname}} {{Auth::user()->lastname}}</h5>
      <p>@if(Auth::user()->role == 1) Super-Admin  @elseif(Auth::user()->role == 2) Administrador @else Estudiante @endif</p>
    </div>
    <div class="userbox">
        <ul class="list-inline mb-0">
            <li class="list-inline-item"><a href="#" class="profile-icon"><img src="{{asset('assets/images/svg-icon/user.svg')}}" class="img-fluid" alt="user"></a></li>
            <li class="list-inline-item"><a href="#" class="profile-icon"><img src="{{asset('assets/images/svg-icon/email.svg')}}" class="img-fluid" alt="email"></a></li>
            <li class="list-inline-item"><a class="profile-icon"><img src="{{asset('assets/images/svg-icon/logout.svg')}}" class="img-fluid" alt="logout" href="{{ route('logout') }}" v-on:click="logout()"></a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
              @csrf
          </form>           
              </li>
        </ul>
      </div>
</div>