<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;

class UsersController extends Controller
{
    public function __construct() {
        $this->middleware('auth:sanctum');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::all();

        return $user;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        if (
            !$request->password ||
            !$request->firstname ||
            !$request->lastname ||
            !$request->email ||
            !$request->password_confirmation
        ) {
            $error = ["error" => "Debe rellenar todos los campos necesarios para continuar creando al nuevo usuario."];
            return response($error);
        }
        $user = User::where('email',$request->email)->first();
        if($user){
            $error = ["error" => "El correo electrónico ingresado ya se encuentra registrado. pruebe con un nuevo correo."];
            return response($error);
        }elseif (strlen($request->password) < 8) {
            $error = ["error" => "La contraseña ingresada no cumple con los requisitos minimos necesarios de seguridad."];
            return response($error);
                
        }elseif (
            $request->password != $request->password_confirmation
        ) {
            $error = ["error" => "Las contraseñas ingresadas no coincide con la verificación."];
            return response($error);
                
        }

        $user = User::create([
            'firstname' => $request['firstname'],
            'lastname' => $request['lastname'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
            'role' => $request['role']
        ]);
        return $user;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        return $id;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        return $user;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        $user = User::find($id);
        $request->validate([
            'firstname' => ['required', 'string', 'max:255'],
            'lastname' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255']
        ]);
        
            if($request->password){
                if ($request->password_confirmation) {
                    if($request->password === $request->password_confirmation){
                        $user->password = Hash::make($request->password);
                    }else{
                        $error = ['error' => 'La contraseña no coincide con su confirmación'];
                        return $error;
                    }
                }else{
                    $error = ['error' => 'La contraseña no coincide con su confirmación'];
                    return $error;
                }
            }
            $user->firstname = $request->firstname;
            $user->lastname = $request->lastname;
            $user->email = $request->email;
            $user->save();
            return $user;
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id)->delete();
        return $user;
    }
}
