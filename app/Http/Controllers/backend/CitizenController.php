<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class CitizenController extends Controller
{
    public function country()
    {
        $json = Storage::disk('local')->get('countries/json/countries.json');
       
        return $json;
    }
    public function states()
    {
        $json = Storage::disk('local')->get('countries/json/states.json');
       
        return $json;
    }
}
