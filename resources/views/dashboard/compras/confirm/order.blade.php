@extends('layouts.dashboard.app')
@section('content')
@include('layouts.dashboard.assets.rightbar.breadcrumbbar_sinboton')

<div class="contentbar">                
    <!-- Start row -->
    <div class="row">
        <!-- Start col -->
        <div class="col-md-12 col-lg-12 col-xl-12">
            <div class="card m-b-30">
                <div class="card-header">
                <h5 class="card-title">Confirmación de pago de la planilla de inscripción #{{$order->id}} </h5>
                </div>
                <div class="card-body">
                    <div class="row justify-content-center">
                        <div class="col-lg-7">
                            <dashboard-compras-order-component :orders="{{$order}}" :auth="{{Auth::user()}}"></dashboard-compras-order-component>
                        </div>
                        <div class="col-lg-5">
                        <dashboard-pagos-confirmation-info-component :pagos='{{$order->pagos}}' :bancos="{{$order->pagos[0]->banco}}" :users="{{$order->users}}" :order="{{$order->id}}"></dashboard-pagos-confirmation-info-component>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End col -->
    </div>
    <!-- End row -->
</div>    

@endsection