<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\PromoCode;
use Illuminate\Http\Request;

class PromoCodeController extends Controller
{
    public function __construct() {
        $this->middleware('auth:sanctum');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $code = PromoCode::all();
        return $code;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        
        $promo = PromoCode::where('promo_code',$request->promo_code)->first();

        if($promo) {
            return ['error' => 'El Codigo promocional ya existe', 'code' => 409];
        }else{
            
            $promo = PromoCode::create([
                'promo_code'    => $request['promo_code'],
                'percent_desc'  => $request['percent_desc'],
                'value_desc'    => $request['value_desc'],
                'status'        => $request['status'],
                'expiration_date' => $request['expiration_date']
            ]);
            return [
                'code' => 200,
                'promo_code' => $promo
            ];
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PromoCode  $promoCode
     * @return \Illuminate\Http\Response
     */
    public function show($promo)
    {
        $code = PromoCode::where('promo_code',$promo)->first();
        if($code){
            $today = date('yy-m-d');
            if($code->expiration_date >= $today){ 
                return $code;
            }else{ 
                return [
                    'error' => 409 , 
                    'message' => 'El codigo promocional está vencido', 
                    'code' => $code
                ]; 
            }
            
        }else{
            return ['error' => 409 , 'message' => 'El codigo promocional no existe'];
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PromoCode  $promoCode
     * @return \Illuminate\Http\Response
     */
    public function edit(PromoCode $promoCode)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PromoCode  $promoCode
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PromoCode $promoCode)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PromoCode  $promoCode
     * @return \Illuminate\Http\Response
     */
    public function destroy(PromoCode $promoCode)
    {
        //
    }
}
