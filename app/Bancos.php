<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bancos extends Model
{
    protected $fillable = [
        'name_banco',
        'tipo_cuenta',
        'numero_cuenta',
        'nombre_persona',
        'documento',
        'documento_type',
        'email'
    ];
}
