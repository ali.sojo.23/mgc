@extends('layouts.dashboard.app')

@section('content')

<!-- Start Breadcrumbbar -->                    
@include('layouts.dashboard.assets.rightbar.breadcrumbbar')
<!-- End Breadcrumbbar -->
<!-- Start Contentbar -->    
<div class="contentbar">                
    <!-- Start row -->
    <div class="row">
        
        <!-- Start col -->
        <div class="col-lg-12">
            <div class="card m-b-30">
                <div class="card-header">
                    <h5 class="card-title">Tabla de datos de estudiantes registrados en la plataforma.</h5>
                </div>
                <div class="card-body">
                    <h6 class="card-subtitle">Exportar datos en múltiples formatos.</h6>
                    <div class="table-responsive">
                    <datatable-users-component :role="{{$role}}" ></datatable-users-component>
                    </div>
                </div>
            </div>
        </div>
        <!-- End col -->
    </div>
    <!-- End row -->
</div>
<!-- End Contentbar -->
@endsection