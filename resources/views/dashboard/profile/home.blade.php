@extends('layouts.dashboard.app')

@section('content')
<!-- Start Breadcrumbbar -->                    
<!-- End Breadcrumbbar -->
<!-- Start Contentbar -->    
<div style="margin-top: 6rem" class="contentbar">                
    <!-- Start row -->
    <div class="row">
        <!-- Start col -->
        <div class="col-lg-5 col-xl-3">
            <div class="card m-b-30">
                <div class="card-header">                                
                    <h5 class="card-title mb-0">Mi perfil</h5>                                       
                </div>
                <div class="card-body">
                    @if(Auth::user()->role == 1)
                        @include('dashboard.profile.nav.adminnav')
                        @include('dashboard.profile.nav.usernav')
                    @elseif(Auth::user()->role ==2)
                        @include('dashboard.profile.nav.adminnav')
                    @else
                        @include('dashboard.profile.nav.usernav')
                    @endif
                </div>
            </div>
        </div>
        <!-- End col -->
        <!-- Start col -->
    <my-account-component role="{{Auth::user()->role}}" user="{{Auth::user()}}"></my-account-component>
    @if(Auth::user()->role == 10)
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form> 
    @endif
        <!-- End col -->
    </div>
    <!-- End row -->                  
</div>
<!-- End Contentbar -->
@endsection
