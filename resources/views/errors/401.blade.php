@extends('layouts.errors.app',['title' => '401'])

@section('title', __('Unauthorized'))
@section('code', '401')
@section('message', __('Unauthorized'))
