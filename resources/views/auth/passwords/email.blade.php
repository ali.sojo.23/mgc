@extends('layouts.auth.app')

@section('content')
<style>
    .auth-box.login-box .auth-box-right .card {
        padding-bottom: 188px 
    }
    @media (max-width: 767px){
        .auth-box.login-box .auth-box-right .card {
        padding-bottom: 0px 
    }
    }
</style>
<div class="card-body">
    
    <form method="POST" action="{{ route('password.email') }}">
        @csrf
        <h4 class="text-primary mb-4">¿Olvidó su contraseña?</h4>
        <p class="mb-4">Ingrese la dirección de correo electrónico a continuación para recibir instrucciones para restablecer la contraseña.</p>
        <div class="form-group">
            <input type="email"  class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Ingrese su email" autofocus>

            @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>                          
      <button type="submit" class="btn btn-success btn-lg btn-block font-18">Enviar correo de recuperación de contraseña</button>
    </form>
<p class="mb-0 mt-3">¿Recordó su contraseña? <a href="{{route('login')}}">Inicie sesión</a></p>
</div>

@endsection
