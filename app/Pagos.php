<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pagos extends Model
{
    protected $fillable = [
        'order_id',
        'banco_id',
        'other_bank',
        'other_bank_name',
        'referencia',
        'nombre',
        'documento',
        'correo_inscripcion'
    ];

    public function banco()
    {
        return $this->hasOne('App\Bancos','id','banco_id');
    }
}
