<div style="background-color: transparent;">
    <div
        class="block-grid"
        style="
            margin: 0 auto;
            min-width: 320px;
            max-width: 650px;
            overflow-wrap: break-word;
            word-wrap: break-word;
            word-break: break-word;
            background-color: transparent;
        "
    >
        <div
            style="
                border-collapse: collapse;
                display: table;
                width: 100%;
                background-color: transparent;
            "
        >
            <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:transparent;"><tr><td align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:650px"><tr class="layout-full-width" style="background-color:transparent"><![endif]-->
            <!--[if (mso)|(IE)]><td align="center" width="650" style="background-color:transparent;width:650px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 0px; padding-top:20px; padding-bottom:60px;"><![endif]-->
            <div
                class="col num12"
                style="
                    min-width: 320px;
                    max-width: 650px;
                    display: table-cell;
                    vertical-align: top;
                    width: 650px;
                "
            >
                <div style="width: 100% !important;">
                    <!--[if (!mso)&(!IE)]><!-->
                    <div
                        style="
                            border-top: 0px solid
                                transparent;
                            border-left: 0px solid
                                transparent;
                            border-bottom: 0px solid
                                transparent;
                            border-right: 0px solid
                                transparent;
                            padding-top: 20px;
                            padding-bottom: 60px;
                            padding-right: 0px;
                            padding-left: 0px;
                        "
                    >
                        <!--<![endif]-->
                        <table
                            cellpadding="0"
                            cellspacing="0"
                            class="social_icons"
                            role="presentation"
                            style="
                                table-layout: fixed;
                                vertical-align: top;
                                border-spacing: 0;
                                border-collapse: collapse;
                                mso-table-lspace: 0pt;
                                mso-table-rspace: 0pt;
                            "
                            valign="top"
                            width="100%"
                        >
                            <tbody>
                                <tr
                                    style="
                                        vertical-align: top;
                                    "
                                    valign="top"
                                >
                                    <td
                                        style="
                                            word-break: break-word;
                                            vertical-align: top;
                                            padding-top: 10px;
                                            padding-right: 10px;
                                            padding-bottom: 10px;
                                            padding-left: 10px;
                                        "
                                        valign="top"
                                    >
                                        <table
                                            align="center"
                                            cellpadding="0"
                                            cellspacing="0"
                                            class="social_table"
                                            role="presentation"
                                            style="
                                                table-layout: fixed;
                                                vertical-align: top;
                                                border-spacing: 0;
                                                border-collapse: collapse;
                                                mso-table-tspace: 0;
                                                mso-table-rspace: 0;
                                                mso-table-bspace: 0;
                                                mso-table-lspace: 0;
                                            "
                                            valign="top"
                                        >
                                            <tbody>
                                                <tr
                                                    align="center"
                                                    style="
                                                        vertical-align: top;
                                                        display: inline-block;
                                                        text-align: center;
                                                    "
                                                    valign="top"
                                                >
                                                                                                                        
                                                    <td
                                                        style="
                                                            word-break: break-word;
                                                            vertical-align: top;
                                                            padding-bottom: 5px;
                                                            padding-right: 8px;
                                                            padding-left: 8px;
                                                        "
                                                        valign="top"
                                                    >
                                                        <a
                                                            href="https://www.instagram.com/academy_megalca/"
                                                            target="_blank"
                                                            ><img
                                                                alt="Instagram"
                                                                height="32"
                                                                src="{{asset('email/instagram2x.png')}}"
                                                                style="
                                                                    text-decoration: none;
                                                                    -ms-interpolation-mode: bicubic;
                                                                    height: auto;
                                                                    border: 0;
                                                                    display: block;
                                                                "
                                                                title="Instagram"
                                                                width="32"
                                                        /></a>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px; font-family: Tahoma, Verdana, sans-serif"><![endif]-->
                        <div
                            style="
                                color: #555555;
                                font-family: Lato,
                                    Tahoma, Verdana,
                                    Segoe, sans-serif;
                                line-height: 1.5;
                                padding-top: 10px;
                                padding-right: 10px;
                                padding-bottom: 10px;
                                padding-left: 10px;
                            "
                        >
                            <div
                                style="
                                    font-size: 12px;
                                    line-height: 1.5;
                                    font-family: Lato,
                                        Tahoma, Verdana,
                                        Segoe,
                                        sans-serif;
                                    color: #555555;
                                    mso-line-height-alt: 18px;
                                "
                            >
                                <p
                                    style="
                                        font-size: 14px;
                                        line-height: 1.5;
                                        text-align: center;
                                        font-family: Lato,
                                            Tahoma,
                                            Verdana,
                                            Segoe,
                                            sans-serif;
                                        word-break: break-word;
                                        mso-line-height-alt: 21px;
                                        margin: 0;
                                    "
                                >
                                Megalca Academy - Donde el aprendizaje es tu mejor inversión.
                                </p>
                                <p
                                    style="
                                        font-size: 14px;
                                        line-height: 1.5;
                                        text-align: center;
                                        font-family: Lato,
                                            Tahoma,
                                            Verdana,
                                            Segoe,
                                            sans-serif;
                                        word-break: break-word;
                                        mso-line-height-alt: 21px;
                                        margin: 0;
                                    "
                                >
                                Avenida mariño n°141. Edificio Aragua local -18, Maracay, Aragua, 2106
                                </p>
                            </div>
                        </div>
                        <!--[if mso]></td></tr></table><![endif]-->
                        <table
                            border="0"
                            cellpadding="0"
                            cellspacing="0"
                            class="divider"
                            role="presentation"
                            style="
                                table-layout: fixed;
                                vertical-align: top;
                                border-spacing: 0;
                                border-collapse: collapse;
                                mso-table-lspace: 0pt;
                                mso-table-rspace: 0pt;
                                min-width: 100%;
                                -ms-text-size-adjust: 100%;
                                -webkit-text-size-adjust: 100%;
                            "
                            valign="top"
                            width="100%"
                        >
                            <tbody>
                                <tr
                                    style="
                                        vertical-align: top;
                                    "
                                    valign="top"
                                >
                                    <td
                                        class="divider_inner"
                                        style="
                                            word-break: break-word;
                                            vertical-align: top;
                                            min-width: 100%;
                                            -ms-text-size-adjust: 100%;
                                            -webkit-text-size-adjust: 100%;
                                            padding-top: 10px;
                                            padding-right: 10px;
                                            padding-bottom: 10px;
                                            padding-left: 10px;
                                        "
                                        valign="top"
                                    >
                                        <table
                                            align="center"
                                            border="0"
                                            cellpadding="0"
                                            cellspacing="0"
                                            class="divider_content"
                                            height="0"
                                            role="presentation"
                                            style="
                                                table-layout: fixed;
                                                vertical-align: top;
                                                border-spacing: 0;
                                                border-collapse: collapse;
                                                mso-table-lspace: 0pt;
                                                mso-table-rspace: 0pt;
                                                border-top: 1px
                                                    dotted
                                                    #c4c4c4;
                                                height: 0px;
                                                width: 60%;
                                            "
                                            valign="top"
                                            width="60%"
                                        >
                                            <tbody>
                                                <tr
                                                    style="
                                                        vertical-align: top;
                                                    "
                                                    valign="top"
                                                >
                                                    <td
                                                        height="0"
                                                        style="
                                                            word-break: break-word;
                                                            vertical-align: top;
                                                            -ms-text-size-adjust: 100%;
                                                            -webkit-text-size-adjust: 100%;
                                                        "
                                                        valign="top"
                                                    >
                                                        <span></span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px; font-family: Tahoma, Verdana, sans-serif"><![endif]-->
                        <div
                            style="
                                color: #4f4f4f;
                                font-family: Lato,
                                    Tahoma, Verdana,
                                    Segoe, sans-serif;
                                line-height: 1.2;
                                padding-top: 10px;
                                padding-right: 10px;
                                padding-bottom: 10px;
                                padding-left: 10px;
                            "
                        >
                            <div
                                style="
                                    font-size: 12px;
                                    line-height: 1.2;
                                    font-family: Lato,
                                        Tahoma, Verdana,
                                        Segoe,
                                        sans-serif;
                                    color: #4f4f4f;
                                    mso-line-height-alt: 14px;
                                "
                            >
                                <p
                                    style="
                                        font-size: 14px;
                                        line-height: 1.2;
                                        text-align: center;
                                        font-family: Lato,
                                            Tahoma,
                                            Verdana,
                                            Segoe,
                                            sans-serif;
                                        word-break: break-word;
                                        mso-line-height-alt: 17px;
                                        margin: 0;
                                    "
                                >
                                    <span
                                        style="
                                            font-size: 14px;
                                        "
                                        >&copy; @php
                                            echo date('Y')
                                        @endphp
                                            <strong
                                            ><a
                                                href="#"
                                                rel="noopener"
                                                style="
                                                    text-decoration: none;
                                                    color: #2190e3;
                                                "
                                                target="https://megalcaacademy.com/"
                                                >Megalca Academy</a
                                            >
                                        </strong>
                                        |
                                        
                                        <span
                                            style="
                                                background-color: transparent;
                                                font-size: 14px;
                                            "
                                            >Make with love ♥ by</span
                                        ></span
                                    >
                                    <strong
                                            ><a
                                                href="#"
                                                rel="noopener"
                                                style="
                                                    text-decoration: none;
                                                    color: #2190e3;
                                                "
                                                target="https://akweb-solutions.com/"
                                                >GRUPO AKWEB C.A.</a
                                            >
                                        </strong>
                                </p>
                            </div>
                        </div>
                        <!--[if mso]></td></tr></table><![endif]-->
                        <!--[if (!mso)&(!IE)]><!-->
                    </div>
                    <!--<![endif]-->
                </div>
            </div>
            <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
            <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
        </div>
    </div>
</div>