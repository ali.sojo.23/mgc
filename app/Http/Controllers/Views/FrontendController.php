<?php

namespace App\Http\Controllers\Views;

use App\curso_categories;
use App\cursos;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class FrontendController extends Controller
{

    public function index(){
        $category = curso_categories::all();
        return view('frontend.index',[
            'category'=> $category,
            'title'=>'Cursos disponible', 
            "scripts" => [
                
            ]]);
    }
    public function curso($id)
    {
        $curso = cursos::where('slug_id',$id)->with('categorias')->first();
        if($curso){
            return view('frontend.curso',[
                'curso' => $curso,
                'title'=>$curso->title, 
                "scripts" => [
                    'assets/plugins/slick/slick.min.js',
                    "assets/js/custom/custom-ecommerce-single-product.js"
                ]]);
        }else{
            abort(404);
        }
        
    }
    public function cart()
    {
        return view('frontend.cart',[
            'title'=>'Planilla de inscripción', 
            "scripts" => [
                'assets/plugins/slick/slick.min.js',
                "assets/js/custom/custom-ecommerce-single-product.js"
            ]]);
    }
    public function checkout()
    {
        return view('frontend.checkout',[
            'title'=>'Procesar Pagos', 
            "scripts" => [
                "assets\plugins\jquery-step\jquery.steps.min.js",
                "assets\js\custom\custom-form-wizard.js",
            ],
            'paypal' => [
            ]]);
    }
}
