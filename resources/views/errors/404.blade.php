@extends('layouts.errors.app',['title' => '404'])

@section('title', __('Not Found'))
@section('content')

<div class="text-center">
    <img src="{{asset('assets\images\logo.svg')}}" class="img-fluid error-logo" alt="logo">
    <img src="{{asset('assets\images\error\404.svg')}}" class="img-fluid error-image" alt="404">
    <h4 class="error-subtitle mb-4">Oops! la página solicitada no ha sido encontrada.</h4>
    <p class="mb-4">Nosotros no encontramos la página que está buscando. Por favor regrese a página anterior o visite nuestro sitio web.</p>
    <a href="javascript:void(0)" onclick="window.history.go(-1); return false;" class="btn btn-primary font-16"><i class="feather icon-home mr-2"></i> Regresar a la página anterior</a>
</div>

@endsection
