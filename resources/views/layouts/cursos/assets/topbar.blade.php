<div class="topbar">
    <!-- Start row -->
    <div class="row align-items-center">
        <!-- Start col -->
        <div class="col-md-12 align-self-center">
            <div class="togglebar">
                <ul class="list-inline mb-0">
                    <li class="list-inline-item">
                        <div class="logobar py-0">
                            <a href="{{route('index')}}" class="logo logo-large"><img src="{{asset('assets/images/logo.svg')}}" class="img-fluid" alt="logo" height="40px"></a>
                            
                        </div>
                    </li>
                </ul>
            </div>
            <div class="infobar">
                <ul class="list-inline mb-0 mt-3">
                    <li class="list-inline-item">
                        <div class="notifybar">
                            <a :style="elementInCar ? 'background-color:rgba(244, 68, 85, 0.1)' : ''" href="{{route('cart')}}" class="infobar-icon"> 
                                <img src="{{asset('assets/images/svg-icon/ecommerce.svg')}}" class="img-fluid" alt="notifications">
                                <span v-if="elementInCar" style="bottom:.9rem;background-color:red" class="live-icon"></span>
                            </a>
                        </div>
                    </li>
                    @auth
                    <li class="list-inline-item">
                        <div class="settingbar">
                            <a href="{{route('dashboard')}}" class="infobar-icon">
                                <img src="{{asset('assets/images/svg-icon/dashboard.svg')}}" class="img-fluid" alt="settings">
                            </a>
                            
                        </div>
                    </li>
                    @else
                    <li class="list-inline-item">
                        <div class="settingbar">
                            <a href="{{route('login')}}" target="_blank" class="infobar-icon">
                                <img src="{{asset('assets/images/svg-icon/user.svg')}}" class="img-fluid" alt="settings">
                            </a>
                            
                        </div>
                    </li>
                    @endauth
                </ul>
            </div>
        </div>
        <!-- End col -->
    </div> 
    <!-- End row -->
</div>