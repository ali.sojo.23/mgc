<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class informacion extends Model
{
    protected $fillable = ['user_id',
    'birthday',
    'whatsapp',
    'pais',
    'ciudad'];
}
