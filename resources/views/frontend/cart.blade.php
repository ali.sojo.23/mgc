@extends('layouts.cursos.app')
@section('content')
    <!-- Start row -->
    <div class="row">
        <!-- Start col -->
        <div class="col-md-12 col-lg-12 col-xl-12">
            <div class="card m-b-30">
                <div class="card-header">
                <h5 class="card-title">Planilla de inscripción de cursos en Megalca Academy </h5>
                </div>
                <div class="card-body">
                    <div class="row justify-content-center">
                        <div class="col-lg-10 col-xl-8">
                            <front-planilla-component @auth
                                
                             :auth={{Auth::user()}} @verifycar="verifyShopCar" @else :auth="false" @endauth></front-planilla-component>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End col -->
    </div>
    <!-- End row -->
@endsection