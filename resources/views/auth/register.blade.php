@extends('layouts.auth.app')

@section('content')
<style>
    .auth-box .auth-box-left .card .card-body {
    padding: 55px !important;
}
</style>
<div class="card-body">
    <form method="POST" action="{{ route('register') }}">
        @csrf
        <h4 class="text-primary mb-4">¡Completar datos de inscripción!</h4>
        <div class="form-group">
            <input type="text" placeholder="Ingrese su nombre" class="form-control @error('firstname') is-invalid @enderror" name="firstname" value="{{ old('firstname') }}" required autocomplete="fisrtname" autofocus>
            @error('firstname')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
        <div class="form-group">
            <input type="text" placeholder="Ingrese su apellido" class="form-control @error('lastname') is-invalid @enderror" name="lastname" value="{{ old('lastname') }}" required autocomplete="lastname" autofocus>
            @error('lastname')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
        <div class="form-group">
            <input type="email" placeholder="Ingrese su correo electrónico" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
            @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
        <div class="form-group">
            <input type="password" placeholder="Ingrese su contraseña" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

            @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
        <div class="form-group">
            <input type="password"  placeholder="Reingrese su contraseña" class="form-control" name="password_confirmation" required autocomplete="new-password">
        </div>
        <div class="form-row mb-3">
            <div class="col-sm-12">
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="terms">
                    <label class="custom-control-label font-14" for="terms">Acepto los terminos y condiciones establecidos por Megalca Academy</label>
                </div>                                
            </div>
        </div>                          
      <button type="submit" class="btn btn-success btn-lg btn-block font-18">Inscribirse</button>
    </form>
    <p class="mb-0 mt-3">¿Ya posee una cuenta en Megalca Academy? <a href="{{route('login')}}">Iniciar sesión</a></p>
</div>
@endsection
