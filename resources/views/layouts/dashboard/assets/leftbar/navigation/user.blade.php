<li class="vertical-header">Main</li>
<li>
    <a href="javaScript:void();">
        <img src="{{asset('assets\images\svg-icon\dashboard.svg')}}" class="img-fluid" alt="dashboard"><span>Dashboard</span><i class="feather icon-chevron-right pull-right"></i>
    </a>
    <ul class="vertical-submenu">
        <li><a href="index.html"><i class="mdi mdi-circle"></i>Social Media</a></li>
        <li><a href="dashboard-ecommerce.html"><i class="mdi mdi-circle"></i>eCommerce</a></li>
        <li><a href="dashboard-analytics.html"><i class="mdi mdi-circle"></i>Analytics</a></li>
    </ul>
</li>