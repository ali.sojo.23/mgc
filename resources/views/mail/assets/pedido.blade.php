<div style="background-color: transparent;">
    <div
        class="block-grid"
        style="
            margin: 0 auto;
            min-width: 320px;
            max-width: 650px;
            overflow-wrap: break-word;
            word-wrap: break-word;
            word-break: break-word;
            background-color: #ffffff;
        "
    >
        <div
            style="
                border-collapse: collapse;
                display: table;
                width: 100%;
                background-color: #ffffff;
            "
        >
            <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:transparent;"><tr><td align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:650px"><tr class="layout-full-width" style="background-color:#FFFFFF"><![endif]-->
            <!--[if (mso)|(IE)]><td align="center" width="650" style="background-color:#FFFFFF;width:650px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 0px; padding-top:15px; padding-bottom:5px;"><![endif]-->
            <div
                class="col num12"
                style="
                    min-width: 320px;
                    max-width: 650px;
                    display: table-cell;
                    vertical-align: top;
                    width: 650px;
                "
            >
                <div style="width: 100% !important;">
                    <!--[if (!mso)&(!IE)]><!-->
                    <div
                        style="
                            border-top: 0px solid
                                transparent;
                            border-left: 0px solid
                                transparent;
                            border-bottom: 0px solid
                                transparent;
                            border-right: 0px solid
                                transparent;
                            padding-top: 15px;
                            padding-bottom: 5px;
                            padding-right: 0px;
                            padding-left: 0px;
                        "
                    >
                        <!--<![endif]-->
                        <!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px; font-family: Tahoma, Verdana, sans-serif"><![endif]-->
                        <div
                            style="
                                color: #052d3d;
                                font-family: Lato,
                                    Tahoma, Verdana,
                                    Segoe, sans-serif;
                                line-height: 1.2;
                                padding-top: 10px;
                                padding-right: 10px;
                                padding-bottom: 10px;
                                padding-left: 10px;
                            "
                        >
                            <div
                                style="
                                    line-height: 1.2;
                                    font-size: 12px;
                                    font-family: Lato,
                                        Tahoma, Verdana,
                                        Segoe,
                                        sans-serif;
                                    color: #052d3d;
                                    mso-line-height-alt: 14px;
                                "
                            >
                                <p
                                    style="
                                        line-height: 1.2;
                                        text-align: center;
                                        font-size: 20px;
                                        font-family: Lato,
                                            Tahoma,
                                            Verdana,
                                            Segoe,
                                            sans-serif;
                                        word-break: break-word;
                                        mso-line-height-alt: 24px;
                                        margin: 0;
                                    "
                                >
                                    <span
                                        style="
                                            font-size: 20px;
                                        "
                                        >Planilla de inscripción # {{$order->id}}</span
                                    >
                                </p>
                            </div>
                        </div>
                        <!--[if mso]></td></tr></table><![endif]-->
                        <!--[if (!mso)&(!IE)]><!-->
                    </div>
                    <!--<![endif]-->
                </div>
            </div>
            <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
            <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
        </div>
    </div>
</div>