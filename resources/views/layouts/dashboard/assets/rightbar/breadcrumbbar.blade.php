<div class="breadcrumbbar">
    <div class="row align-items-center">
        <div class="col-md-8 col-lg-8">
            <h4 class="page-title">{{$title ?? ''}}</h4>
            <div class="breadcrumb-list">
                <ol class="breadcrumb">
                    @foreach($breadcrumbs as $item)
                        @if($loop->last)
                        <li class="breadcrumb-item active" aria-current="page">{{$item}}</li>
                        @elseif ($loop->first)
                        <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{$item}}</a></li>
                        @else
                        <li class="breadcrumb-item"><a href="#">{{$item}}</a></li>
                        @endif
                    @endforeach
                    
                    
                    
                </ol>
            </div>
        </div>
        <div class="col-md-4 col-lg-4">
            <div class="widgetbar">
                <button class="btn btn-primary" data-toggle="modal"
                data-target="#crearModal">Agregar Nuevo</button>
            </div>                        
        </div>
    </div>          
</div>