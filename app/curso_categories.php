<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class curso_categories extends Model
{
    protected $fillable = [
        'name'
    ];
    protected $hidden = [
        'created_at',
        'updated_at'
    ];
    public function cursos()
    {
        return $this->hasMany('App\cursos','category_id','id');
    }
}
