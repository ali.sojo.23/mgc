<div class="topbar-mobile">
    <div class="row align-items-center">
        <div class="col-md-12">
            <div class="mobile-logobar">
                <a href="{{route('index')}}" class="mobile-logo"><img src="{{asset('assets\images\logo.svg')}}" class="img-fluid" alt="logo"></a>
            </div>
            <div class="mobile-togglebar">
                <ul class="list-inline mb-0 mt-3">
                    <li class="list-inline-item">
                        <div class="topbar-toggle-icon">
                            <a class="topbar-toggle-hamburger" href="javascript:void();">
                                <img src="{{asset('assets\images\svg-icon\horizontal.svg')}}" class="img-fluid menu-hamburger-horizontal" alt="horizontal">
                               <img src="{{asset('assets\images\svg-icon\verticle.svg')}}" class="img-fluid menu-hamburger-vertical" alt="verticle">
                             </a>
                         </div>
                    </li>                            
                </ul>
            </div>
        </div>
    </div>
</div>