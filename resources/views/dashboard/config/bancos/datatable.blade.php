@extends('layouts.dashboard.app')

@section('content')
<!-- Start Breadcrumbbar -->                    
@include('layouts.dashboard.assets.rightbar.breadcrumbbar')
<!-- End Breadcrumbbar -->
<!-- Start Contentbar -->    
<div class="contentbar">                
    <!-- Start row -->
    <div class="row">
        
        <!-- Start col -->
        <div class="col-lg-12">
            <div class="card m-b-30">
                <div class="card-body">
                    <h6 class="card-subtitle">Exportar datos en multiples formatos.</h6>
                    <div class="table-responsive">
                    <dashboard-config-bancos-datatable-component :bancos="{{$bancos}}"></dashboard-config-bancos-datatable-component>
                    </div>
                </div>
            </div>
        </div>
        <!-- End col -->
    </div>
    <!-- End row -->
</div>
<!-- End Contentbar -->
@endsection