<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Mail\inscripcion;
use App\orders;
use App\Pagos;
use App\Mail\Invoices;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;

class PagosController extends Controller
{
    public function __construct() {
        $this->middleware('auth:sanctum');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $orden = orders::find($request['order_id']);
        if($orden){
            $pago = Pagos::create([
                'order_id' => $request['order_id'],
                'banco_id' => $request['banco_id'],
                'other_bank' => $request['other_bank'],
                'other_bank_name' => $request['other_bank_name'],
                'referencia' => $request['referencia'],
                'nombre' => $request['nombre'],
                'documento' => $request['documento'],
                'correo_inscripcion' => $request['correo_inscripcion']
            ]);

            if($pago){
                $orden->status = 1;
                $orden->save();

                $orden->users;
                $status = ['code' => 1, 'message' => 'EN PROCESO'];
                foreach($orden->items as $item){
                    $item->cursos;
                }
                Mail::to($orden->users->email, $orden->users->firstname . ' ' . $orden->users->lastname)
                        ->send(new Invoices($orden,$status));
                return $pago;
            }
        }else{
            abort(404);
        }
        
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Pagos  $pagos
     * @return \Illuminate\Http\Response
     */
    public function show(Pagos $pagos)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Pagos  $pagos
     * @return \Illuminate\Http\Response
     */
    public function edit(Pagos $pagos)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Pagos  $pagos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pagos $transferencia)
    {
        $order = orders::where('id',$transferencia->order_id)->first();
        if($request['status'] > 3){
            abort(500);
        }
        if($order){
            $order->status = $request['status'];
            $order->save();
            $order->users;
            foreach($order->items as $item){
                $item->cursos;
            }
            if($request['status'] == 2){
                $status = ['code' => 2, 'message' => 'COMPLETADA', 'asunto' => 'El pago de tu inscripción #'. $order->id .' fue procesada'];
                Mail::to($order->users->email, $order->users->firstname . ' ' . $order->users->lastname)
                        ->send(new inscripcion($order,$status));
            }
            if($request['status'] == 3){
                $status = ['code' => 3, 'message' => 'COMPLETADA', 'asunto' => 'Tu inscripción #'. $order->id .' fue procesada con exito'];
                Mail::to($order->users->email, $order->users->firstname . ' ' . $order->users->lastname)
                        ->send(new inscripcion($order,$status));
            }
        }else{
            abort(404);
        }

        return $order;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Pagos  $pagos
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pagos $pagos)
    {
        //
    }
}
