<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PromoCode extends Model
{
    protected $fillable = [
        'promo_code',
        'percent_desc',
        'value_desc',
        'status',
        'expiration_date'
    ];
}
