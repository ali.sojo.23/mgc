<?php

namespace App\Http\Controllers\Views;

use App\Bancos;
use App\curso_categories;
use App\Http\Controllers\Controller;
use App\orders;
use App\PromoCode;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('dashboard.profile.home', [
            'title' => 'Mi perfil de usuario',
            'styles' => [
                "assets\plugins\select2\select2.min.css",
                "assets\plugins\switchery\switchery.min.css",
                "assets\plugins\animate\animate.css",
                "assets\css\bootstrap.min.css",
                "assets\css\icons.css",
                "assets\css\style.css"
            ],
            'scripts' => [
                "assets\plugins\select2\select2.min.js",

            ],
            'breadcrumbs' => [
                'Dashboard',
            ]
        ]);
    }
    public function userAdmin()
    {
        return view('dashboard.users.datatable', [
            'title' => 'Usuarios Administrativos',
            'role' => '2',
            'styles' => [
                "assets\plugins\switchery\switchery.min.css",
                "assets\plugins\animate\animate.css",
                "assets\css\bootstrap.min.css",
                "assets\css\icons.css",
                "assets\css\style.css",
                'assets/plugins/datatables/dataTables.bootstrap4.min.css',
                'assets/plugins/datatables/buttons.bootstrap4.min.css',
                'assets/plugins/datatables/responsive.bootstrap4.min.css'

            ],
            'scripts' => [
                'assets/plugins/datatables/jquery.dataTables.min.js',
                'assets/plugins/datatables/dataTables.bootstrap4.min.js',
                'assets/plugins/datatables/dataTables.buttons.min.js',
                'assets/plugins/datatables/buttons.bootstrap4.min.js',
                'assets/plugins/datatables/jszip.min.js',
                'assets/plugins/datatables/pdfmake.min.js',
                'assets/plugins/datatables/vfs_fonts.js',
                'assets/plugins/datatables/buttons.html5.min.js',
                'assets/plugins/datatables/buttons.print.min.js',
                'assets/plugins/datatables/buttons.colVis.min.js',
                'assets/plugins/datatables/dataTables.responsive.min.js',
                'assets/plugins/datatables/responsive.bootstrap4.min.js',
                'assets/js/custom/custom-table-datatable.js',
            ],
            'breadcrumbs' => [
                'Dashboard',
                'Usuarios',
                'Administradores'
            ]
        ]);
    }
    public function userUser(Request $request)
    {
        return view('dashboard.users.datatable', [
            'title' => 'Estudiantes',
            'role' => '10',
            'styles' => [
                "assets\plugins\switchery\switchery.min.css",
                "assets\plugins\animate\animate.css",
                "assets\css\bootstrap.min.css",
                "assets\css\icons.css",
                "assets\css\style.css",
                'assets/plugins/datatables/dataTables.bootstrap4.min.css',
                'assets/plugins/datatables/buttons.bootstrap4.min.css',
                'assets/plugins/datatables/responsive.bootstrap4.min.css'

            ],
            'scripts' => [
                'assets/plugins/datatables/jquery.dataTables.min.js',
                'assets/plugins/datatables/dataTables.bootstrap4.min.js',
                'assets/plugins/datatables/dataTables.buttons.min.js',
                'assets/plugins/datatables/buttons.bootstrap4.min.js',
                'assets/plugins/datatables/jszip.min.js',
                'assets/plugins/datatables/pdfmake.min.js',
                'assets/plugins/datatables/vfs_fonts.js',
                'assets/plugins/datatables/buttons.html5.min.js',
                'assets/plugins/datatables/buttons.print.min.js',
                'assets/plugins/datatables/buttons.colVis.min.js',
                'assets/plugins/datatables/dataTables.responsive.min.js',
                'assets/plugins/datatables/responsive.bootstrap4.min.js',
                'assets/js/custom/custom-table-datatable.js',
                'assets\js\custom\custom-model.js'
            ],
            'breadcrumbs' => [
                'Dashboard',
                'Usuarios',
                'Estudiantes'
            ]
        ]);
    }
    public function cursosCategory(Request $request)
    {
        return view('dashboard.cursos.categorias.datatable', [
            'title' => 'Cursos en Moodle',
            'curso' => 'moodle',
            'styles' => [
                "assets\plugins\switchery\switchery.min.css",
                "assets\plugins\animate\animate.css",
                "assets\css\bootstrap.min.css",
                "assets\css\icons.css",
                "assets\css\style.css",
                'assets/plugins/datatables/dataTables.bootstrap4.min.css',
                'assets/plugins/datatables/buttons.bootstrap4.min.css',
                'assets/plugins/datatables/responsive.bootstrap4.min.css'

            ],
            'scripts' => [
                'assets/plugins/datatables/jquery.dataTables.min.js',
                'assets/plugins/datatables/dataTables.bootstrap4.min.js',
                'assets/plugins/datatables/dataTables.buttons.min.js',
                'assets/plugins/datatables/buttons.bootstrap4.min.js',
                'assets/plugins/datatables/jszip.min.js',
                'assets/plugins/datatables/pdfmake.min.js',
                'assets/plugins/datatables/vfs_fonts.js',
                'assets/plugins/datatables/buttons.html5.min.js',
                'assets/plugins/datatables/buttons.print.min.js',
                'assets/plugins/datatables/buttons.colVis.min.js',
                'assets/plugins/datatables/dataTables.responsive.min.js',
                'assets/plugins/datatables/responsive.bootstrap4.min.js',
                'assets/js/custom/custom-table-datatable.js',
                'assets\js\custom\custom-model.js'
            ],
            'breadcrumbs' => [
                'Dashboard',
                'Cursos',
                'Disponibles en el Moodle'
            ]
        ]);
    }
    public function cursosDisponibles(Request $request)
    {
        return view('dashboard.cursos.disponible.datatable', [
            'title' => 'Cursos en Moodle',
            'curso' => 'disponible',
            'styles' => [
                "assets\plugins\switchery\switchery.min.css",
                "assets\plugins\animate\animate.css",
                "assets\css\bootstrap.min.css",
                "assets\css\icons.css",
                "assets\css\style.css",
                'assets/plugins/datatables/dataTables.bootstrap4.min.css',
                'assets/plugins/datatables/buttons.bootstrap4.min.css',
                'assets/plugins/datatables/responsive.bootstrap4.min.css'

            ],
            'scripts' => [
                'assets/plugins/datatables/jquery.dataTables.min.js',
                'assets/plugins/datatables/dataTables.bootstrap4.min.js',
                'assets/plugins/datatables/dataTables.buttons.min.js',
                'assets/plugins/datatables/buttons.bootstrap4.min.js',
                'assets/plugins/datatables/jszip.min.js',
                'assets/plugins/datatables/pdfmake.min.js',
                'assets/plugins/datatables/vfs_fonts.js',
                'assets/plugins/datatables/buttons.html5.min.js',
                'assets/plugins/datatables/buttons.print.min.js',
                'assets/plugins/datatables/buttons.colVis.min.js',
                'assets/plugins/datatables/dataTables.responsive.min.js',
                'assets/plugins/datatables/responsive.bootstrap4.min.js',
                'assets/js/custom/custom-table-datatable.js',
                'assets\js\custom\custom-model.js'
            ],
            'breadcrumbs' => [
                'Dashboard',
                'Cursos',
                'Disponibles en venta.'
            ]
        ]);
    }
    public function ordersCompletes(Request $request)
    {
        

        return view('dashboard.compras.complete.datatable', [
            'title' => 'Plantillas de inscripción generadas y pagadas',
            'order' => 3,
            'styles' => [
                "assets\plugins\switchery\switchery.min.css",
                "assets\plugins\animate\animate.css",
                "assets\css\bootstrap.min.css",
                "assets\css\icons.css",
                "assets\css\style.css",
                'assets/plugins/datatables/dataTables.bootstrap4.min.css',
                'assets/plugins/datatables/buttons.bootstrap4.min.css',
                'assets/plugins/datatables/responsive.bootstrap4.min.css'

            ],
            'scripts' => [
                'assets/plugins/datatables/jquery.dataTables.min.js',
                'assets/plugins/datatables/dataTables.bootstrap4.min.js',
                'assets/plugins/datatables/dataTables.buttons.min.js',
                'assets/plugins/datatables/buttons.bootstrap4.min.js',
                'assets/plugins/datatables/jszip.min.js',
                'assets/plugins/datatables/pdfmake.min.js',
                'assets/plugins/datatables/vfs_fonts.js',
                'assets/plugins/datatables/buttons.html5.min.js',
                'assets/plugins/datatables/buttons.print.min.js',
                'assets/plugins/datatables/buttons.colVis.min.js',
                'assets/plugins/datatables/dataTables.responsive.min.js',
                'assets/plugins/datatables/responsive.bootstrap4.min.js',
                'assets/js/custom/custom-table-datatable.js',
                'assets\js\custom\custom-model.js'
            ],
            'breadcrumbs' => [
                'Dashboard',
                'Compras',
                'Planillas completadas'
            ]
        ]);
    }
    public function ordersPendings(Request $request)
    {

        return view('dashboard.compras.complete.datatable', [
            'title' => 'Plantillas de inscripción generadas y pendientes de pago',
            'order' => 0,
            'styles' => [
                "assets\plugins\switchery\switchery.min.css",
                "assets\plugins\animate\animate.css",
                "assets\css\bootstrap.min.css",
                "assets\css\icons.css",
                "assets\css\style.css",
                'assets/plugins/datatables/dataTables.bootstrap4.min.css',
                'assets/plugins/datatables/buttons.bootstrap4.min.css',
                'assets/plugins/datatables/responsive.bootstrap4.min.css'

            ],
            'scripts' => [
                'assets/plugins/datatables/jquery.dataTables.min.js',
                'assets/plugins/datatables/dataTables.bootstrap4.min.js',
                'assets/plugins/datatables/dataTables.buttons.min.js',
                'assets/plugins/datatables/buttons.bootstrap4.min.js',
                'assets/plugins/datatables/jszip.min.js',
                'assets/plugins/datatables/pdfmake.min.js',
                'assets/plugins/datatables/vfs_fonts.js',
                'assets/plugins/datatables/buttons.html5.min.js',
                'assets/plugins/datatables/buttons.print.min.js',
                'assets/plugins/datatables/buttons.colVis.min.js',
                'assets/plugins/datatables/dataTables.responsive.min.js',
                'assets/plugins/datatables/responsive.bootstrap4.min.js',
                'assets/js/custom/custom-table-datatable.js',
                'assets\js\custom\custom-model.js'
            ],
            'breadcrumbs' => [
                'Dashboard',
                'Compras',
                'Pendientes por pago'
            ]
        ]);
    }
    public function ordersPendingsForVerication(Request $request)
    {
        
        return view('dashboard.compras.verification.datatable', [
            'title' => 'Plantillas de inscripción generadas y pendientes de pago',
            'order' => 1,
            'styles' => [
                "assets\plugins\switchery\switchery.min.css",
                "assets\plugins\animate\animate.css",
                "assets\css\bootstrap.min.css",
                "assets\css\icons.css",
                "assets\css\style.css",
                'assets/plugins/datatables/dataTables.bootstrap4.min.css',
                'assets/plugins/datatables/buttons.bootstrap4.min.css',
                'assets/plugins/datatables/responsive.bootstrap4.min.css'

            ],
            'scripts' => [
                'assets/plugins/datatables/jquery.dataTables.min.js',
                'assets/plugins/datatables/dataTables.bootstrap4.min.js',
                'assets/plugins/datatables/dataTables.buttons.min.js',
                'assets/plugins/datatables/buttons.bootstrap4.min.js',
                'assets/plugins/datatables/jszip.min.js',
                'assets/plugins/datatables/pdfmake.min.js',
                'assets/plugins/datatables/vfs_fonts.js',
                'assets/plugins/datatables/buttons.html5.min.js',
                'assets/plugins/datatables/buttons.print.min.js',
                'assets/plugins/datatables/buttons.colVis.min.js',
                'assets/plugins/datatables/dataTables.responsive.min.js',
                'assets/plugins/datatables/responsive.bootstrap4.min.js',
                'assets/js/custom/custom-table-datatable.js',
                'assets\js\custom\custom-model.js'
            ],
            'breadcrumbs' => [
                'Dashboard',
                'Compras',
                'Pendientes por verificación'
            ]
        ]);
    }
    public function ordersPendingsForInscripcion(Request $request)
    {
        
        return view('dashboard.compras.verification.datatable', [
            'title' => 'Plantillas de inscripción procesada y pendientes de inscripción',
            'order' => 2,
            'styles' => [
                "assets\plugins\switchery\switchery.min.css",
                "assets\plugins\animate\animate.css",
                "assets\css\bootstrap.min.css",
                "assets\css\icons.css",
                "assets\css\style.css",
                'assets/plugins/datatables/dataTables.bootstrap4.min.css',
                'assets/plugins/datatables/buttons.bootstrap4.min.css',
                'assets/plugins/datatables/responsive.bootstrap4.min.css'

            ],
            'scripts' => [
                'assets/plugins/datatables/jquery.dataTables.min.js',
                'assets/plugins/datatables/dataTables.bootstrap4.min.js',
                'assets/plugins/datatables/dataTables.buttons.min.js',
                'assets/plugins/datatables/buttons.bootstrap4.min.js',
                'assets/plugins/datatables/jszip.min.js',
                'assets/plugins/datatables/pdfmake.min.js',
                'assets/plugins/datatables/vfs_fonts.js',
                'assets/plugins/datatables/buttons.html5.min.js',
                'assets/plugins/datatables/buttons.print.min.js',
                'assets/plugins/datatables/buttons.colVis.min.js',
                'assets/plugins/datatables/dataTables.responsive.min.js',
                'assets/plugins/datatables/responsive.bootstrap4.min.js',
                'assets/js/custom/custom-table-datatable.js',
                'assets\js\custom\custom-model.js'
            ],
            'breadcrumbs' => [
                'Dashboard',
                'Compras',
                'Pendientes',
                'Inscripción'
            ]
        ]);
    }
    public function bancos(Request $request)
    {
        $bancos = Bancos::all();
        return view('dashboard.config.bancos.datatable', [
            'title' => 'Plantillas de inscripción generadas y pendientes de pago',
            'bancos' => $bancos,
            'styles' => [
                "assets\plugins\switchery\switchery.min.css",
                "assets\plugins\animate\animate.css",
                "assets\css\bootstrap.min.css",
                "assets\css\icons.css",
                "assets\css\style.css",
                'assets/plugins/datatables/dataTables.bootstrap4.min.css',
                'assets/plugins/datatables/buttons.bootstrap4.min.css',
                'assets/plugins/datatables/responsive.bootstrap4.min.css'

            ],
            'scripts' => [
                'assets/plugins/datatables/jquery.dataTables.min.js',
                'assets/plugins/datatables/dataTables.bootstrap4.min.js',
                'assets/plugins/datatables/dataTables.buttons.min.js',
                'assets/plugins/datatables/buttons.bootstrap4.min.js',
                'assets/plugins/datatables/jszip.min.js',
                'assets/plugins/datatables/pdfmake.min.js',
                'assets/plugins/datatables/vfs_fonts.js',
                'assets/plugins/datatables/buttons.html5.min.js',
                'assets/plugins/datatables/buttons.print.min.js',
                'assets/plugins/datatables/buttons.colVis.min.js',
                'assets/plugins/datatables/dataTables.responsive.min.js',
                'assets/plugins/datatables/responsive.bootstrap4.min.js',
                'assets/js/custom/custom-table-datatable.js',
                'assets\js\custom\custom-model.js'
            ],
            'breadcrumbs' => [
                'Dashboard',
                'Compras',
                'Pendientes por verificación'
            ]
        ]);
    }
    public function viewOrder(orders $order, Request $request)
    {

        if ($order) {
            if ($request->user()->role < 5 || $request->user()->id == $order->user_id) {
                $order->cupon;
                foreach ($order->items as $item) {
                    $item->cursos;
                }

                return view('dashboard.compras.show.order', [
                    'title' => 'Cursos en Moodle',
                    'order' => $order,
                    'styles' => [
                        "assets\plugins\switchery\switchery.min.css",
                        "assets\plugins\animate\animate.css",
                        "assets\css\bootstrap.min.css",
                        "assets\css\icons.css",
                        "assets\css\style.css",
                        'assets/plugins/datatables/dataTables.bootstrap4.min.css',
                        'assets/plugins/datatables/buttons.bootstrap4.min.css',
                        'assets/plugins/datatables/responsive.bootstrap4.min.css'
                    ],
                    "scripts" => [
                        'assets/plugins/slick/slick.min.js',
                        "assets/js/custom/custom-ecommerce-single-product.js"
                    ],
                    'breadcrumbs' => [
                        'Dashboard',
                        'Compras',
                        'Confirmar pago'
                    ]
                ]);
            } else {
                abort(403);
            }
        } else {
            abort(404);
        }
    }
    public function RegistrarPagos(Request $request)
    {
        $orders = orders::where([
            'user_id' => $request->user()->id,
            'status'  => 0
        ])->get();
        $bancos = Bancos::all();

        return view('dashboard.pagos.verification.form', [
            'title' => 'Cursos en Moodle',
            'bancos' => $bancos,
            'orders' => $orders,
            'styles' => [
                "assets\plugins\switchery\switchery.min.css",
                "assets\plugins\animate\animate.css",
                "assets\css\bootstrap.min.css",
                "assets\css\icons.css",
                "assets\css\style.css",
            ],
            "scripts" => [
                "assets\js\custom\custom-switchery.js"
            ],
            'breadcrumbs' => [
                'Dashboard',
                'Compras',
                'Registrar pagos'
            ]
        ]);
    }
    public function confirmarPago($order)
    {
        $orders = orders::where('id', $order)->with('pagos', 'items', 'users')->first();
        if ($orders->status != 1) {
            abort(403);
        }
        $orders->cupon;
        foreach ($orders->items as $item) {
            $item->cursos;
        }
        foreach ($orders->pagos as $item) {
            $item->banco;
        }

        return view('dashboard.compras.confirm.order', [
            'title' => 'Cursos en Moodle',
            'order' => $orders,
            'styles' => [
                "assets\plugins\switchery\switchery.min.css",
                "assets\plugins\animate\animate.css",
                "assets\css\bootstrap.min.css",
                "assets\css\icons.css",
                "assets\css\style.css",
            ],
            "scripts" => [
                "assets\js\custom\custom-switchery.js"
            ],
            'breadcrumbs' => [
                'Dashboard',
                'Compras',
                'Registrar pagos'
            ]
        ]);
    }

    public function confirmarInsripcion($order)
    {
        $orders = orders::where('id', $order)->with('pagos', 'items', 'users')->first();
        if ($orders->status != 2) {
            abort(403);
        }
        $orders->cupon;
        foreach ($orders->items as $item) {
            $item->cursos;
        }
        foreach ($orders->pagos as $item) {
            $item->banco;
        }

        return view('dashboard.compras.inscripcion.order', [
            'title' => 'Cursos en Moodle',
            'order' => $orders,
            'styles' => [
                "assets\plugins\switchery\switchery.min.css",
                "assets\plugins\animate\animate.css",
                "assets\css\bootstrap.min.css",
                "assets\css\icons.css",
                "assets\css\style.css",
            ],
            "scripts" => [
                "assets\js\custom\custom-switchery.js"
            ],
            'breadcrumbs' => [
                'Dashboard',
                'Compras',
                'Registrar pagos'
            ]
        ]);
    }
    public function configurarPromoCode()
    {
        $code = PromoCode::all();
        return view('dashboard.config.PromoCode.datatable', [
            'title' => 'Configuración codigo promocional',
            'code' => $code,
            'styles' => [
                "assets\plugins\switchery\switchery.min.css",
                "assets\plugins\animate\animate.css",
                "assets\css\bootstrap.min.css",
                "assets\css\icons.css",
                "assets\css\style.css",
            ],
            "scripts" => [
                
            ],
            "date" => "assets\plugins\datepicker\datepicker.min.js",
            'breadcrumbs' => [
                'Dashboard',
                'Administración',
                'Promo Codes'
            ]
        ]);
    }
}
