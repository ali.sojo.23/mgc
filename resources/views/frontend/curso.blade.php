@extends('layouts.cursos.app') 
@section('content')
@error('firstname')
<div
    class="alert alert-danger alert-dismissible fade show"
    role="alert"
>
    <strong>Ohh no!</strong> {{$message}}.
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@enderror
@error('lastname')
<div
    class="alert alert-danger alert-dismissible fade show"
    role="alert"
>
    <strong>Ohh no!</strong> {{$message}}.
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@enderror
@error('email')
<div
    class="alert alert-danger alert-dismissible fade show"
    role="alert"
>
    <strong>Ohh no!</strong> {{$message}}.
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@enderror
@error('password')
<div
    class="alert alert-danger alert-dismissible fade show"
    role="alert"
>
    <strong>Ohh no!</strong> {{$message}}.
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@enderror
<!-- Start row -->
<front-curso-first-row-component
    :iniciar="'{{ route('login') }}'"
    :registro="'{{ route('register') }}'"
    :token="'{{ csrf_token() }}'"
    :cursos="{{ $curso }}"
    @verifycar="verifyShopCar"
    @auth:user="{{Auth::user()->id}}"
    @endauth
></front-curso-first-row-component>
<!-- End row -->
<!-- Start row -->
<div class="row">
    <!-- Start col -->
    <div class="col-lg-12">
        <div class="card m-b-30">
            <div class="card-header">
                <h5 class="card-title">Detalles del curso</h5>
            </div>
            <div class="card-body">
                <ul
                    class="nav nav-tabs custom-tab-line mb-3"
                    id="defaultTabLine"
                    role="tablist"
                >
                    <li class="nav-item">
                        <a
                            class="nav-link active"
                            id="description-tab-line"
                            data-toggle="tab"
                            href="#description-line"
                            role="tab"
                            aria-controls="description-line"
                            aria-selected="true"
                            ><i class="feather icon-file-text mr-2"></i
                            >Descripción del curso</a
                        >
                    </li>
                </ul>
                <div class="tab-content" id="defaultTabContentLine">
                    <div
                        class="tab-pane fade show active"
                        id="description-line"
                        role="tabpanel"
                        aria-labelledby="description-tab-line"
                    >
                        {!! $curso->description !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End col -->
</div>
<!-- End row -->
<!-- Start row -->
<front-curso-second-row-component
    :category="{{$curso->categorias->id}}"
    :curso="{{$curso->id}}"
></front-curso-second-row-component>
<!-- End row -->
@endsection
