<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class inscripcion extends Mailable
{
    use Queueable, SerializesModels;

    public $ord;
    public $state;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($order, $status)
    {
        $this->ord = $order;
        $this->state = $status;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.inscripcion')
                    ->with('order', $this->ord)
                    ->with('status',$this->state)
                    ->from('no-reply@megalcaacademy.com','Megalca Academy')
                    ->subject($this->state['asunto']);
    }
}
