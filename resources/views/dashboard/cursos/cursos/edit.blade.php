@extends('layouts.dashboard.app')
@section('content')
<style>
    .contentbar{
        margin-top: 5rem;
    }
</style>
<!-- Start Breadcrumbbar -->                    
<div class="contentbar">
    <!-- Start row -->
<dashbard-cursos-edit-component :data="{{$curso}}"></dashbard-cursos-edit-component>
    <!-- End row -->
</div>
@endsection