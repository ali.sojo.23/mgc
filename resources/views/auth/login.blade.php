@extends('layouts.auth.app')

@section('content')
<div class="card-body">
<form method="POST" action="{{ route('login') }}">
        @csrf
        <h4 class="text-primary mb-4">¡Iniciar sesión!</h4>
        <div class="form-group">
            <input type="email" class="form-control @error('email') is-invalid @enderror"" id="username" placeholder="Ingrese su correo elecrónico" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
            @error('email')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
        </div>
        <div class="form-group">
            <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Ingrese su contraseña" required autocomplete="current-password">
            @error('password')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
        </div>
        <div class="form-row mb-3">
            <div class="col-sm-6">
                <div class="custom-control custom-checkbox">
                  <input type="checkbox" class="custom-control-input" id="rememberme" {{ old('remember') ? 'checked' : '' }}>
                  <label class="custom-control-label font-14" for="rememberme">Recordar datos</label>
                </div>                                
            </div>
            @if (Route::has('password.request'))
            <div class="col-sm-6">
              <div class="forgot-psw"> 
                <a id="forgot-psw" href="{{ route('password.request')}}" class="font-14">¿Olvidó su contraseña?</a>
              </div>
            </div>
            @endif
        </div>                          
      <button type="submit" class="btn btn-success btn-lg btn-block font-18">Iniciar sesión</button>
    </form>
    
    <p class="mb-0 mt-3">¿No estás inscrito aún? <a href="{{route('register')}}">Regístrate</a></p>
    <div class="login-or" style="visibility: hidden">
        <h6 class="text-muted">OR</h6>
    </div>
    <div class="social-login text-center" style="visibility: hidden">
        <button type="submit" class="btn btn-primary-rgba btn-lg btn-block font-18"><i class="mdi mdi-facebook mr-2"></i>Log in with Facebook</button>
        <button type="submit" class="btn btn-danger-rgba btn-lg btn-block font-18"><i class="mdi mdi-google mr-2"></i>Log in with Google</button>
    </div>
</div>

@endsection
