<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="author" content="akweb.com.ve">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <title>@yield('title') | {{ config('app.name', 'Laravel') }}</title>
    <!-- Fevicon -->
    <link rel="shortcut icon" href="{{asset('assets/images/favicon.png')}}" type="image/png">
    <!-- Start CSS -->
    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/css/icons.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/css/style.css')}}" rel="stylesheet" type="text/css">
    <!-- End CSS -->
</head>
<body class="vertical-layout">
    <!-- Start Container -->
    <div id="containerbar" class="containerbar authenticate-bg">
        <!-- Start Container -->
        <div class="container">
            <div class="auth-box error-box">
                <!-- Start row -->
                 <div class="row no-gutters align-items-center justify-content-center">
                    <!-- Start col -->
                    <div class="col-md-8 col-lg-6">
                        @yield('content')
                        @if($title != 404)
                        <div class="text-center">
                            <img src="{{asset('assets/images/logo.svg')}}" class="img-fluid error-logo" alt="logo">
                            <img src="{{asset('assets/images/error/internal-server.svg')}}" class="img-fluid error-image" alt="500">
                            <h4 class="error-subtitle mb-4"> Erro @yield('code')</h4>
                            <p class="mb-4">@yield('message')</p>                           
                            <a href="javascript:void(0)" onclick="window.history.go(-1); return false;" class="btn btn-primary font-16"><i class="feather icon-home mr-2"></i> Regresar</a>
                        </div>
                        @endif
                    </div>
                    <!-- End col -->
                </div>
                <!-- End row -->
            </div>
        </div>
        <!-- End Container -->
    </div>
    <!-- End Containerbar -->
    <!-- Start js -->        
    <script src="{{asset('assets/js/jquery.min.js')}}"></script>
    <script src="{{asset('assets/js/popper.min.js')}}"></script>
    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/js/modernizr.min.js')}}"></script>
    <script src="{{asset('assets/js/detect.js')}}"></script> 
    <script src="{{asset('assets/js/jquery.slimscroll.js')}}"></script>
    <!-- End js -->
</body>
</html>