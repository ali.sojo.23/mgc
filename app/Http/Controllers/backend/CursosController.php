<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\cursos;
use Illuminate\Http\Request;

class CursosController extends Controller
{
    public function __construct() {
        $this->middleware('auth:sanctum')->except('index','show');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $curso = cursos::with('categorias')->get();
        return $curso;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.cursos.cursos.create',[
            'title'=>'Agregar disponibilidad de venta de cursos',
            'styles' => [
                "assets/plugins/switchery/switchery.min.css",
                "assets/plugins/summernote/summernote-bs4.css",
                "assets/plugins/dropzone/dist/dropzone.css",
                "assets/css/bootstrap.min.css",
                "assets/css/icons.css",
                "assets/css/flag-icon.min.css",
                "assets/css/style.css"
            ],
            'scripts' => [
                "assets\plugins\summernote\summernote-bs4.min.js",
                "assets\plugins\dropzone\dist\dropzone.js",
                "assets\js\custom\custom-ecommerce-product-detail-page.js"
            ],
            'breadcrumbs' => [
                'Dashboard',
                'Cursos',
                'Agregar Disponibilidad de cursos'
            ]
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $curso = cursos::where('moodle_id',$request->moodle_id)->first();
        $slug = str_replace(' ','_',$request['title']);
        $slug = strtolower($slug).'_'.$request['moodle_id'];
        if($curso){
            abort(409);
        }else{
            
            $curso = cursos::create([
                'moodle_id' =>    $request['moodle_id'],
                'slug_id'   =>    $slug,
                'category_id'=>   $request['category_id'][0],
                'title'     =>    $request['title'],
                'description'=>   $request['description'],
                'feature_image'=> $request['feature_image'],
                'price'     =>    $request['price'],
                'price_sale'=>    $request['price_sale'],
                'status'=>        $request['status']
            ]);

            return $curso;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\cursos  $cursos
     * @return \Illuminate\Http\Response
     */
    public function show($cursos)
    {
        $curso = cursos::find($cursos);
        $curso->categorias;
        return $curso;
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\cursos  $cursos
     * @return \Illuminate\Http\Response
     */
    public function edit(cursos $curso)
    {
        $curso->categorias;
        
        return view('dashboard.cursos.cursos.edit',[
            'curso'=> $curso,
            'title'=>'Agregar disponibilidad de venta de cursos',
            'styles' => [
                "assets/plugins/switchery/switchery.min.css",
                "assets/plugins/summernote/summernote-bs4.css",
                "assets/plugins/dropzone/dist/dropzone.css",
                "assets/css/bootstrap.min.css",
                "assets/css/icons.css",
                "assets/css/flag-icon.min.css",
                "assets/css/style.css"
            ],
            'scripts' => [
                "assets\plugins\summernote\summernote-bs4.min.js",
                "assets\plugins\dropzone\dist\dropzone.js",
                "assets\js\custom\custom-ecommerce-product-detail-page.js"
            ],
            'breadcrumbs' => [
                'Dashboard',
                'Cursos',
                'Agregar Disponibilidad de cursos'
            ]
            ]);
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\cursos  $cursos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, cursos $cursos)
    {
        if($cursos){
            
                $cursos->moodle_id =    $request['moodle_id'];
                $cursos->category_id=   $request['category_id'][0];
                $cursos->title     =    $request['title'];
                $cursos->description=   $request['description'];
                $cursos->feature_image= $request['feature_image'];
                $cursos->price     =    $request['price'];
                $cursos->price_sale=    $request['price_sale'];
                $cursos->status=        $request['status'];
            $cursos->save();
            return $cursos;
        }else{
            abort(403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\cursos  $cursos
     * @return \Illuminate\Http\Response
     */
    public function destroy(cursos $cursos)
    {
        //
    }
}
