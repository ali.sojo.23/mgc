/*
------------------------------------
    : Custom - Form Wizards js :
------------------------------------
*/
"use strict";
$(document).ready(function() {
    var form = $("#basic-form-wizard");
    form.children("div").steps({
        headerTag: "h3",
        bodyTag: "section",
        transitionEffect: "slideLeft",
        enableFinishButton: false
    });

    var verticalform = $("#vertical-form-wizard");
    verticalform.children("div").steps({
        headerTag: "h3",
        bodyTag: "section",
        transitionEffect: "slideLeft",
        stepsOrientation: "vertical",
        onFinishing: function(event, currentIndex) {
            return verticalform;
        },
        onFinished: function(event, currentIndex) {
            $("#vertical-form-wizard").submit();
        }
    });
    $("#basic-form-wizard .steps").prepend(
        "<div class='form-wizard-line'></div>"
    );
});
