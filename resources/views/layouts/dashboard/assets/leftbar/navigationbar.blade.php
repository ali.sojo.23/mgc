<div class="navigationbar">
    <ul class="vertical-menu">
       @if(Auth::user()->role == 1)
           @include('layouts.dashboard.assets.leftbar.navigation.admin')
           @include('layouts.dashboard.assets.leftbar.navigation.user')
        @elseif(Auth::user()->role == 2)
            @include('layouts.dashboard.assets.leftbar.navigation.admin')
        @endif                                      
    </ul>
</div>