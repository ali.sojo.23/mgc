@extends('layouts.dashboard.app')

@section('content')
<!-- Start Breadcrumbbar -->                    
@include('layouts.dashboard.assets.rightbar.breadcrumbbar_sinboton')

<!-- End Breadcrumbbar -->
<!-- Start Contentbar -->    
<div class="contentbar">                
    <!-- Start row -->
    <div class="row">
        
        <!-- Start col -->
        <div class="col-lg-12">
            <div class="card m-b-30">
                <div class="card-header">
                    <h5 class="card-title">Registro de pagos</h5>
                </div>
                <div class="card-body mb-4">
                    <div class="row justify-content-center">
                    <dashboard-pagos-verification-form-component :banco="{{$bancos}}" :order="{{$orders}}" :auths="{{ Auth::user()}}"></dashboard-pagos-verification-form-component>
                    <dashboard-pagos-verification-bancos-component :bancos="{{$bancos}}"></dashboard-pagos-verification-bancos-component>
                    </div>
                </div>
            </div> 
        </div>
        <!-- End col -->
    </div>
    <!-- End row -->
</div>
<!-- End Contentbar -->
@endsection