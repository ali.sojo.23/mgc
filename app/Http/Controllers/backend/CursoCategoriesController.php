<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\curso_categories as category;
use Illuminate\Http\Request;

class CursoCategoriesController extends Controller
{
    public function __construct() {
        $this->middleware('auth:sanctum')->except('index','show');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cat = category::all();
        foreach ($cat as $item) {
            $item->cursos;
        }
        return $cat;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $cat = category::create([
            'name' => $request->name
        ]);

        return $cat;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\curso_categories  $curso_categories
     * @return \Illuminate\Http\Response
     */
    public function show(category $category)
    {
        $category->cursos;
        return $category;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\curso_categories  $curso_categories
     * @return \Illuminate\Http\Response
     */
    public function edit(category $curso_categories)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\curso_categories  $curso_categories
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, category $category)
    {
        if($category){
            $category->name = $request['name'];
            $category->save();
            return $category;
        }else{
            abort(404);
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\curso_categories  $curso_categories
     * @return \Illuminate\Http\Response
     */
    public function destroy(category $category)
    {   
        $category->delete();
        return $category;
    }
}
