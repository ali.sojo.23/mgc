<div class="rightbar">
    @if(Auth::user()->role < 10)
        <!-- Start Topbar Mobile -->
        @include('layouts.dashboard.assets.rightbar.topbarmobile')
        <!-- Start Topbar -->
        @include('layouts.dashboard.assets.rightbar.topbar')
        <!-- End Topbar -->
    @else
        <!-- Start Topbar Mobile -->
        @include('layouts.cursos.assets.topbarmobile')
        <!-- Start Topbar -->
        @include('layouts.cursos.assets.topbar')
        <!-- End Topbar -->
    @endif
    <!-- Start Breadcrumbbar -->
    @yield('content')                   
    <!-- Start Footerbar -->
    @include('layouts.dashboard.assets.rightbar.footerbar')
    <!-- End Footerbar -->
</div>