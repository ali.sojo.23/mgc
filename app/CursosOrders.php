<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CursosOrders extends Model
{
    protected $fillable = [
        'order_id',
        'curso_id'
    ];

    public function cursos()
    {
        return $this->hasOne('App\cursos','id','curso_id');
    }
}
