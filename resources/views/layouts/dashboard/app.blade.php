<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="akweb.com.ve">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Titulo -->
    <title>{{$title ?? ''}} @if($title) | @endif Dashboard | {{ config('app.name', 'Laravel') }}</title>
    <!-- Fevicon -->
    <link rel="shortcut icon" href="{{asset('assets/images/favicon.png')}}" type="image/png">

    <!-- Styles -->
    
    @foreach($styles as $style)
        <link href="{{ asset($style) }}" rel="stylesheet">
    @endforeach
     <!-- Scripts -->
     @if(Auth::user()->role == 10) 
     <style>
        .topbar,.footerbar{
            left: 0px;
        }
        .rightbar{
            margin: auto
        }
        .live-icon{
            bottom: 25px
        }
        .descount{
            text-decoration: line-through;
            zoom: 60%;
        }
        @media (max-width: 767px){
            .w-75{
                width: 100% !important;
            }
        }
        .page-item.page-link.active {
            color: #2d3646;
            background-color: #dcdde1;
            border-color: rgba(0, 0, 0, 0.05);
        }
        .footerbar{
        position: initial

    }
    </style>
    @endif
    
</head>
<body class="vertical-layout">
    @include('layouts.dashboard.assets.notification')

    <div id="containerbar">
        @if(Auth::user()->role != 10) 
            @include('layouts.dashboard.assets.leftbar')
        @endif
        <!-- Start Rightbar -->
        @include('layouts.dashboard.assets.rightbar')
        <!-- End Rightbar -->
    </div>
    <script src="{{asset('assets\js\jquery.min.js')}}"></script>
    <script src="{{asset('assets\js\popper.min.js')}}"></script>
    <script src="{{asset('assets\js\bootstrap.min.js')}}"></script>
    <script src="{{asset('assets\js\modernizr.min.js')}}"></script>
    <script src="{{asset('assets\js\detect.js')}}"></script>
    <script src="{{asset('assets\js\jquery.slimscroll.js')}}"></script>
    <script src="{{asset('assets\js\vertical-menu.js')}}"></script>
    <!-- Switchery js -->
    <script src="{{asset('assets\plugins\switchery\switchery.min.js')}}"></script>
    
    @php
        $datepicker = $date ?? ''
    @endphp
    @if ($datepicker)
    <script src="{{asset( $datepicker )}}"></script>
    @endif
    <!-- Core js -->
    <script src="{{asset('js/app.js?version='.env('APP_VERSION'))}}"></script>

    @foreach($scripts as $script)
        <script src="{{asset($script)}}"></script>
    @endforeach
    <script src="{{asset('assets\js\core.js')}}"></script>
    <!-- End js -->
</body>
</html>
