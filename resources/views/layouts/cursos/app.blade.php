<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="akweb.com.ve">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Titulo -->
    <title> {{$title ?? ''}} | {{ config('app.name', 'Laravel') }}</title>
    <!-- Fevicon -->
    <link rel="shortcut icon" href="{{asset('assets/images/favicon.png')}}" type="image/png">
    <!-- Start css -->
    <!-- Switchery css -->
    <link href="{{asset('assets/plugins/switchery/switchery.min.css')}}" rel="stylesheet">
    <!-- Slick css -->
    <link href="{{asset('assets/plugins/slick/slick.css')}}" rel="stylesheet">
    <link href="{{asset('assets/plugins/slick/slick-theme.css')}}" rel="stylesheet">    
    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/css/icons.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/css/style.css')}}" rel="stylesheet" type="text/css">

    <style>
        .topbar,.footerbar{
            left: 0px;
        }
        .live-icon{
            bottom: 25px
        }
        .contentbar{
            padding-top: 9rem;
        }
        .descount{
            text-decoration: line-through;
            zoom: 60%;
        }
        @media (max-width: 767px){
            .w-75{
                width: 100% !important;
            }
        }
        .page-item.page-link.active {
            color: #2d3646;
            background-color: #dcdde1;
            border-color: rgba(0, 0, 0, 0.05);
        }
    </style>
    <!-- Facebook Pixel Code -->
    @if (App::environment('local') === true)
    @else
    
    <script>
        !function(f,b,e,v,n,t,s){
            if(f.fbq)return;
            n=f.fbq=function(){
                n.callMethod?
                    n.callMethod.apply(n,arguments):
                    n.queue.push(arguments)
                };
            if(!f._fbq)f._fbq=n;
            n.push=n;
            n.loaded=!0;
            n.version='2.0';
            n.queue=[];
            t=b.createElement(e);
            t.async=!0;
            t.src=v;
            s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)
        }
        (window,document,'script','https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '598978180812528'); 
        fbq('track', 'PageView');
    </script>
    <noscript>
        <img 
            height="1" 
            width="1" 
            src="https://www.facebook.com/tr?id=598978180812528&ev=PageView&noscript=1"
        />
    </noscript>
    @endif
    <!-- End Facebook Pixel Code -->
    <!-- End css -->
</head>
<body class="vertical-layout">
    
    <!-- Start Containerbar -->
    <div id="containerbar">
        <!-- Start Rightbar -->
        <div class="rightbar w-75 mx-auto">
            <!-- Start Topbar Mobile -->
            @include('layouts.cursos.assets.topbarmobile')
            <!-- Start Topbar -->
            @include('layouts.cursos.assets.topbar')
            <!-- End Topbar -->
            <!-- Start Contentbar -->    
            <div class="contentbar">                
                @yield('content')
            </div>
            <!-- End Contentbar -->
            @include('layouts.dashboard.assets.rightbar.footerbar')
        </div>
        <!-- End Rightbar -->
    </div>
    <!-- End Containerbar -->
    <!-- Start js -->        
    <script src="{{asset('assets/js/jquery.min.js')}}"></script>
    <script src="{{asset('assets/js/popper.min.js')}}"></script>
    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/js/modernizr.min.js')}}"></script>
    <script src="{{asset('assets/js/detect.js')}}"></script>
    <script src="{{asset('assets/js/jquery.slimscroll.js')}}"></script>
    <script src="{{asset('assets/js/vertical-menu.js')}}"></script>
    <!-- Switchery js -->
    <script src="{{asset('assets/plugins/switchery/switchery.min.js')}}"></script>
    <!-- Slick js -->
    <script src="{{asset('js/app.js?version='.env('APP_VERSION'))}}"></script>
    @foreach($scripts as $script)
    <script src="{{asset($script)}}"></script>
    @endforeach
    
    <!-- Core js -->
    <script src="{{asset('assets/js/core.js')}}"></script>
    <!-- End js -->
</body>
</html>