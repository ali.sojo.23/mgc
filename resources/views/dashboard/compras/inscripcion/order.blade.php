@extends('layouts.dashboard.app')
@section('content')
@include('layouts.dashboard.assets.rightbar.breadcrumbbar_sinboton')

<div class="contentbar">                
    <!-- Start row -->
    <div class="row">
        <!-- Start col -->
        <div class="col-md-12 col-lg-12 col-xl-12">
            <div class="card m-b-30">
                <div class="card-header">
                <h5 class="card-title">Confirmación de pago de la planilla de inscripción #{{$order->id}} </h5>
                </div>
                <div class="card-body">
                    <div class="row justify-content-center">
                        <div class="col-lg-12">
                            <dashboard-compras-inscripcion-component :orders="{{$order}}" :auth="{{Auth::user()}}"></dashboard-compras-inscripcion-component>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End col -->
    </div>
    <!-- End row -->
</div>    

@endsection