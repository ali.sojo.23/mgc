<?php

use App\Http\Controllers\CursosController;
use App\Http\Controllers\Views\DashboardController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Rutas de inicio de sesion 
Auth::routes();

Route::group(['namespace' => 'Views'], function(){
    
    Route::prefix('/')->group(function () {
        Route::get('/','FrontendController@index')->name('index');
        Route::get('curso/{id?}', 'FrontendController@curso')->name('curso');
        Route::get('cart', 'FrontendController@cart')->name('cart');
        Route::get('checkout', 'FrontendController@checkout')->name('checkout');
    });
    Route::prefix('dashboard')->group(function () {
        Route::get('/', 'DashboardController@index')->name('dashboard');
        Route::group(['prefix' => 'users','middleware'=>'adminRole'],function () {
            Route::get('admin','DashboardController@userAdmin')->name('dashboard.user.admin');
            Route::get('user','DashboardController@userUser')->name('dashboard.user.user');
        });
        Route::group(['prefix' => 'cursos','middleware'=>'adminRole'],function () {
            Route::get('category','DashboardController@cursosCategory')->name('dashboard.cursos.category');
            Route::get('disponibles','DashboardController@cursosDisponibles')->name('dashboard.user.disponibles');
        });
        Route::group(['prefix' => 'compras'],function () {
            Route::get('/completas','DashboardController@ordersCompletes')->name('dashboard.compras.complete')->middleware('adminRole');
            Route::get('/pendientes','DashboardController@ordersPendings')->name('dashboard.compras.pending')->middleware('adminRole');
            Route::get('/verificate/payment','DashboardController@ordersPendingsForVerication')->name('dashboard.compras.verifications')->middleware('adminRole');
            Route::get('/verificate/inscriptions','DashboardController@ordersPendingsForInscripcion')->name('dashboard.compras.inscriptions')->middleware('adminRole');
            Route::get('/plantilla/{order?}/confirmation','DashboardController@confirmarPago')->name('dashboard.compras.confirmarPagos')->middleware('adminRole');
            Route::get('/plantilla/{order?}/inscripcion','DashboardController@confirmarInsripcion')->name('dashboard.compras.confirmarInscripcion')->middleware('adminRole');
            Route::get('/registrar-pagos','DashboardController@RegistrarPagos')->name('dashboard.compras.registroPagos');
            Route::get('/plantilla/{order?}','DashboardController@viewOrder')->name('dashboard.compras.order');
        });
        Route::group(['prefix' => 'configuracion','middleware'=>'adminRole'],function () {
            Route::get('bancos','DashboardController@bancos')->name('dashboard.config.bancos');
            Route::get('promo-code','DashboardController@configurarPromoCode')->name('dashboard.config.PromoCode');
        });
    });    
});
Route::group(['prefix'=>'dashboard','namespace' => 'backend','middleware'=>['adminRole','auth']],function(){
   Route::resource('curso','CursosController')->only([
    'create','edit',
   ]);
   
});

