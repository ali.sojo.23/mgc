<div style="background-color: transparent;">
    <div
        class="block-grid two-up no-stack"
        style="
            margin: 0 auto;
            min-width: 320px;
            max-width: 650px;
            overflow-wrap: break-word;
            word-wrap: break-word;
            word-break: break-word;
            background-color: #ffffff;
        "
    >
        <div
            style="
                border-collapse: collapse;
                display: table;
                width: 100%;
                background-color: #ffffff;
            "
        >
            <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:transparent;"><tr><td align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:650px"><tr class="layout-full-width" style="background-color:#FFFFFF"><![endif]-->
            <!--[if (mso)|(IE)]><td align="center" width="325" style="background-color:#FFFFFF;width:325px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 25px; padding-top:25px; padding-bottom:25px;"><![endif]-->
            <div
                class="col num6"
                style="
                    min-width: 320px;
                    max-width: 325px;
                    display: table-cell;
                    vertical-align: top;
                    width: 325px;
                "
            >
                <div style="width: 100% !important;">
                    <!--[if (!mso)&(!IE)]><!-->
                    <div
                        style="
                            border-top: 0px solid
                                transparent;
                            border-left: 0px solid
                                transparent;
                            border-bottom: 0px solid
                                transparent;
                            border-right: 0px solid
                                transparent;
                            padding-top: 25px;
                            padding-bottom: 25px;
                            padding-right: 0px;
                            padding-left: 25px;
                        "
                    >
                        <!--<![endif]-->
                        <div
                            align="left"
                            class="img-container left fullwidthOnMobile fixedwidth"
                            style="
                                padding-right: 0px;
                                padding-left: 0px;
                            "
                        >
                            <!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr style="line-height:0px"><td style="padding-right: 0px;padding-left: 0px;" align="left"><![endif]-->
                            <div
                                style="
                                    font-size: 1px;
                                    line-height: 5px;
                                "
                            ></div>
                            <img
                                alt="Image"
                                border="0"
                                class="left fullwidthOnMobile fixedwidth"
                                src="{{asset('assets\images\logo.svg')}}"
                                style="
                                    text-decoration: none;
                                    -ms-interpolation-mode: bicubic;
                                    height: auto;
                                    border: 0;
                                    width: 100%;
                                    max-width: 195px;
                                    display: block;
                                "
                                title="Image"
                                width="195"
                            />
                            <!--[if mso]></td></tr></table><![endif]-->
                        </div>
                        <!--[if (!mso)&(!IE)]><!-->
                    </div>
                    <!--<![endif]-->
                </div>
            </div>
            <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
            <!--[if (mso)|(IE)]></td><td align="center" width="325" style="background-color:#FFFFFF;width:325px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 25px; padding-left: 0px; padding-top:25px; padding-bottom:25px;"><![endif]-->
            <div
                class="col num6"
                style="
                    min-width: 320px;
                    max-width: 325px;
                    display: table-cell;
                    vertical-align: top;
                    width: 325px;
                "
            >
                <div style="width: 100% !important;">
                    <!--[if (!mso)&(!IE)]><!-->
                    <div
                        style="
                            border-top: 0px solid
                                transparent;
                            border-left: 0px solid
                                transparent;
                            border-bottom: 0px solid
                                transparent;
                            border-right: 0px solid
                                transparent;
                            padding-top: 25px;
                            padding-bottom: 25px;
                            padding-right: 25px;
                            padding-left: 0px;
                        "
                    >
                        <!--<![endif]-->
                        <div
                            align="right"
                            class="button-container"
                            style="
                                padding-top: 10px;
                                padding-right: 0px;
                                padding-bottom: 10px;
                                padding-left: 10px;
                            "
                        >
                            <!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="border-spacing: 0; border-collapse: collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"><tr><td style="padding-top: 10px; padding-right: 0px; padding-bottom: 10px; padding-left: 10px" align="right"><v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="#" style="height:28.5pt; width:97.5pt; v-text-anchor:middle;" arcsize="37%" stroke="false" fillcolor="#D4E9F9"><w:anchorlock/><v:textbox inset="0,0,0,0"><center style="color:#052d3d; font-family:Tahoma, Verdana, sans-serif; font-size:14px"><!
                            [endif]--><a
                                href="{{asset('/dashboard')}}"
                                style="
                                    -webkit-text-size-adjust: none;
                                    text-decoration: none;
                                    display: inline-block;
                                    color: #052d3d;
                                    background-color: #d4e9f9;
                                    border-radius: 14px;
                                    -webkit-border-radius: 14px;
                                    -moz-border-radius: 14px;
                                    width: auto;
                                    width: auto;
                                    border-top: 1px
                                        solid #d4e9f9;
                                    border-right: 1px
                                        solid #d4e9f9;
                                    border-bottom: 1px
                                        solid #d4e9f9;
                                    border-left: 1px
                                        solid #d4e9f9;
                                    padding-top: 3px;
                                    padding-bottom: 3px;
                                    font-family: Lato,
                                        Tahoma, Verdana,
                                        Segoe,
                                        sans-serif;
                                    text-align: center;
                                    mso-border-alt: none;
                                    word-break: keep-all;
                                "
                                target="_blank"
                                ><span
                                    style="
                                        padding-left: 15px;
                                        padding-right: 15px;
                                        font-size: 14px;
                                        display: inline-block;
                                    "
                                    ><span
                                        style="
                                            font-size: 16px;
                                            line-height: 2;
                                            word-break: break-word;
                                            mso-line-height-alt: 32px;
                                        "
                                        ><span
                                            data-mce-style="font-size: 14px; line-height: 28px;"
                                            style="
                                                font-size: 14px;
                                                line-height: 28px;
                                            "
                                            >Mi cuenta</span
                                        ></span
                                    ></span
                                ></a
                            >
                            <!--[if mso]></center></v:textbox></v:roundrect></td></tr></table><![endif]-->
                        </div>
                        <!--[if (!mso)&(!IE)]><!-->
                    </div>
                    <!--<![endif]-->
                </div>
            </div>
            <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
            <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
        </div>
    </div>
</div>