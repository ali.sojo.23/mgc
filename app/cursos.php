<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cursos extends Model
{
    protected $fillable = [
        'moodle_id',
        'slug_id',
        'category_id',
        'title',
        'description',
        'feature_image',
        'gallery',
        'price',
        'price_sale',
        'status'
    ];
    protected $hidden = [
        'category_id'
    ];

    public function categorias()
    {
        return $this->hasOne('App\curso_categories','id','category_id');
    }
}
