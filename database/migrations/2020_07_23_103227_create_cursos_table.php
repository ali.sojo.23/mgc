<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCursosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cursos', function (Blueprint $table) {
            $table->id();
            $table->string('moodle_id')->unique();
            $table->string('slug_id')->unique();
            $table->bigInteger('category_id')->unsigned()->index()->nullable();  
            $table->string('title');
            $table->longText('description');
            $table->string('feature_image');
            $table->json('gallery')->nullable();
            $table->decimal('price', 8, 2);
            $table->decimal('price_sale', 8, 2)->nullable();
            $table->timestamps();
            $table->foreign('category_id')->references('id')->on('curso_categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cursos');
    }
}
