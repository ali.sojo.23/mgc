<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::insert([
            'firstname' => 'Ali',
            "lastname" => "Sojo",
            'email' => 'ali.sojo.23@gmail.com',
            'role' => '1',
            'password' => Hash::make('password'),
        ]);
       User::insert( [
        'firstname' => 'Williams',
        "lastname" => "Moreno",
        'email' => 'williams.moreno@gmail.com',
        'role' => '2',
        'password' => Hash::make('password'),
        ]);
        User::insert( [
            'firstname' => 'Administrador',
            "lastname" => "Megalca",
            'email' => 'admin@megalcaacademy.com',
            'role' => '2',
            'password' => Hash::make('password'),
         ]);
            User::insert([
                'firstname' => 'Estudiante',
                "lastname" => "Megalca",
                'email' => 'estudiante@megalcaacademy.com',
                'role' => '10',
                'password' => Hash::make('password'),
            ]);
    
    
        // factory(User::class, 15)->create();
    }
}
