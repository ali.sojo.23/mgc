<li class="vertical-header">Administración</li>
{{-- usuarios  --}}
<li>
    <a href="javaScript:void();">
        <img src="{{asset('assets\images\svg-icon\user.svg')}}" class="img-fluid" alt="dashboard"><span>Usuarios</span><i class="feather icon-chevron-right pull-right"></i>
    </a>
    <ul class="vertical-submenu">
        <li><a href="{{route('dashboard.user.admin')}}"><i class="mdi mdi-circle"></i>Administrativos</a></li>
        <li><a href="{{route('dashboard.user.user')}}"><i class="mdi mdi-circle"></i>Estudiantes</a></li>
    </ul>
</li>
{{-- cursos --}}
<li>
    <a href="javaScript:void();">
        <img src="{{asset('assets/images/svg-icon/pages.svg')}}" class="img-fluid" alt="dashboard"><span>Cursos</span><i class="feather icon-chevron-right pull-right"></i>
    </a>
    <ul class="vertical-submenu">
        <li><a href="{{route('dashboard.cursos.category')}}"><i class="mdi mdi-circle"></i>Categorias</a></li>
        <li><a href="{{route('dashboard.user.disponibles')}}"><i class="mdi mdi-circle"></i>Disponibles</a></li>
    </ul>
</li>
{{-- compras --}}
<li>
    <a href="javaScript:void();">
        <img src="{{asset('assets/images/svg-icon/pages.svg')}}" class="img-fluid" alt="dashboard"><span>Inscripciones</span><i class="feather icon-chevron-right pull-right"></i>
    </a>
    <ul class="vertical-submenu">
        <li><a href="{{route('dashboard.compras.pending')}}"><i class="mdi mdi-circle"></i>Pendientes por pago</a></li>
        <li><a href="{{route('dashboard.compras.verifications')}}"><i class="mdi mdi-circle"></i>Pendientes por verificación de pago</a></li>
        <li><a href="{{route('dashboard.compras.inscriptions')}}"><i class="mdi mdi-circle"></i>Pendientes por inscripción</a></li>
        <li><a href="{{route('dashboard.compras.complete')}}"><i class="mdi mdi-circle"></i>Completadas</a></li>
    </ul>
</li>
{{-- configuracion --}}
<li>
    <a href="javaScript:void();">
        <img src="{{asset('assets/images/svg-icon/advanced.svg')}}" class="img-fluid" alt="dashboard"><span>Configuración</span><i class="feather icon-chevron-right pull-right"></i>
    </a>
    <ul class="vertical-submenu">
        <li><a href="{{route('dashboard.config.bancos')}}"><i class="mdi mdi-circle"></i>Bancos</a></li>
        <li><a href="{{route('dashboard.config.PromoCode')}}"><i class="mdi mdi-circle"></i>Promo Codes</a></li>
    </ul>
</li>