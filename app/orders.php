<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\CursosOrders;

class orders extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'user_id',
        'status',
        'price',
        'promo_codes_id'
    ];

    public function items()
    {
        return $this->hasMany('App\CursosOrders','order_id');
    }
    public function users()
    {
        return $this->hasOne('App\User','id','user_id');
    }
    public function pagos()
    {
        return $this->hasMany('App\Pagos','order_id');
    }
    public function cupon()
    {
        return $this->hasOne('App\PromoCode','id','promo_codes_id');
    }
}
