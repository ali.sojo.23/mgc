@extends('layouts.dashboard.app')

@section('content')
<!-- Start Breadcrumbbar -->                    
@include('layouts.dashboard.assets.rightbar.breadcrumbbar_sinboton')

<!-- End Breadcrumbbar -->
<!-- Start Contentbar -->    
<div class="contentbar">                
    <!-- Start row -->
    <div class="row">
        
        <!-- Start col -->
        <div class="col-lg-12">
            <div class="card m-b-30">
                <div class="card-body">
                    <h6 class="card-subtitle">Exportar datos en multiples formatos.</h6>
                    <div class="table-responsive">
                        <dashboard-compras-datatable-component :orders="{{$order}}"></dashboard-compras-datatable-component>
                    </div>
                </div>
            </div>
        </div>
        <!-- End col -->
    </div>
    <!-- End row -->
</div>
<!-- End Contentbar -->
@endsection