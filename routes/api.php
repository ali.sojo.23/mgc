<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['namespace' => 'backend'], function(){
    
    Route::prefix('citizen')->group(function () {
        Route::get('/','CitizenController@country');
        Route::get('states','CitizenController@states');
    });
    Route::prefix('users')->group(function () {
        Route::resource('/','UsersController',['except'=>'show','create','edit'])->parameters([
            '' => 'users'
        ]);
        Route::resource('info','InformacionController',['except'=>'show','create','edit']);
        
    });
    Route::prefix('cursos')->group(function () {
        Route::resource('curso','CursosController')->except(['create','edit'])->parameters([
            'curso' => 'cursos'
        ]);
        Route::resource('category','CursoCategoriesController')->except(['create','edit']);
        
    });
    Route::prefix('accounting')->group(function () {
        Route::resource('orders','OrdersController')->except(['create','edit']);
        Route::resource('payment/transferencias','PagosController')->except(['create','edit']);
    });
    Route::prefix('config')->group(function () {
        Route::resource('bancos','BancosController')->except(['create','edit']);
        Route::resource('promo-code','PromoCodeController')->except(['create','edit']);
    });
    Route::resource('files','FilesController')->only([
        'index','show','store','update','destroy'
       ]);; 
});
