<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Bancos;
use Illuminate\Http\Request;

class BancosController extends Controller
{
    public function __construct() {
        $this->middleware('auth:sanctum');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Bancos::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $banco = Bancos::create([
            'name_banco' => $request['name_banco'],
            'tipo_cuenta' => $request['tipo_cuenta'],
            'numero_cuenta' => $request['numero_cuenta'],
            'nombre_persona' => $request['nombre_persona'],
            'documento' => $request['documento'],
            'documento_type' => $request['documento_type'],
            'email' => $request['email']
        ]);
        return $banco;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Bancos  $bancos
     * @return \Illuminate\Http\Response
     */
    public function show(Bancos $banco)
    {
        return $banco;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Bancos  $bancos
     * @return \Illuminate\Http\Response
     */
    public function edit(Bancos $banco)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Bancos  $bancos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Bancos $banco)
    {
        
        $banco->name_banco = $request['name_banco'];
        $banco->tipo_cuenta = $request['tipo_cuenta'];
        $banco->numero_cuenta = $request['numero_cuenta'];
        $banco->nombre_persona = $request['nombre_persona'];
        $banco->documento = $request['documento'];
        $banco->documento_type = $request['documento_type'];
        $banco->email = $request['email'];

        $banco->save();

        return $banco;

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Bancos  $bancos
     * @return \Illuminate\Http\Response
     */
    public function destroy(Bancos $banco)
    {
        if($banco){
            $banco->delete();
            return $banco;
        }else{
            abort(404);
        }
    }
}
