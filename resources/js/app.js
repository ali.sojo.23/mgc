/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
import LoadScript from "vue-plugin-load-script";
require("./bootstrap");

window.Vue = require("vue");

Vue.use(LoadScript);
import Vuelidate from 'vuelidate'
Vue.use(Vuelidate)
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

// Usuarios
Vue.component(
    "datatable-users-component",
    require("./components/dashboard/users/DatatableComponent.vue").default
);
// Fin de usuarios
//Curso
Vue.component(
    "datatable-cursos-disponible-component",
    require("./components/dashboard/cursos/disponible/DatatableComponent.vue")
        .default
);
Vue.component(
    "datatable-categorias-cursos-component",
    require("./components/dashboard/cursos/categorias/DatatableComponent.vue")
        .default
);
Vue.component(
    "dashbard-cursos-edit-component",
    require("./components/dashboard/cursos/edit/EditComponent.vue").default
);
Vue.component(
    "cursos-component",
    require("./components/dashboard/cursos/create/CreateComponent.vue").default
);
//Fin de Cursos
Vue.component(
    "my-account-component",
    require("./components/dashboard/my-account/my-account.vue").default
);
Vue.component(
    "dashboard-my-account-component",
    require("./components/dashboard/my-account/assets/DashboardComponent.vue")
        .default
);
Vue.component(
    "orders-my-account-component",
    require("./components/dashboard/my-account/assets/OrdersComponent.vue")
        .default
);
Vue.component(
    "address-my-account-component",
    require("./components/dashboard/my-account/assets/AddressComponent.vue")
        .default
);
Vue.component(
    "notifications-my-account-component",
    require("./components/dashboard/my-account/assets/NotificationsComponent.vue")
        .default
);
Vue.component(
    "profile-my-account-component",
    require("./components/dashboard/my-account/assets/ProfileComponent.vue")
        .default
);
Vue.component(
    "cursos-component",
    require("./components/dashboard/cursos/create/CreateComponent.vue").default
);
Vue.component(
    "front-curso-first-row-component",
    require("./components/frontend/curso/FirstComponent.vue").default
);
Vue.component(
    "front-curso-first-auth-login-row-component",
    require("./components/frontend/curso/auth/login.vue").default
);
Vue.component(
    "front-curso-first-auth-register-row-component",
    require("./components/frontend/curso/auth/register.vue").default
);
Vue.component(
    "front-curso-second-row-component",
    require("./components/frontend/curso/SecondComponent.vue").default
);
Vue.component(
    "front-checkout-component",
    require("./components/frontend/checkout/AppComponent.vue").default
);
Vue.component(
    "front-checkout-auth-component",
    require("./components/frontend/checkout/AuthComponent.vue").default
);
Vue.component(
    "front-checkout-pedido-component",
    require("./components/frontend/checkout/ShowComponent.vue").default
);
Vue.component(
    "front-checkout-payment-component",
    require("./components/frontend/checkout/PaymentComponent.vue").default
);
Vue.component(
    "front-checkout-payment-transfer-component",
    require("./components/frontend/checkout/PaymentMethods/TransferComponent.vue")
        .default
);
Vue.component(
    "front-checkout-payment-paypal-component",
    require("./components/frontend/checkout/PaymentMethods/PaypalComponent.vue")
        .default
);
Vue.component(
    "front-index-component",
    require("./components/frontend/index/IndexComponent.vue").default
);
Vue.component(
    "front-planilla-component",
    require("./components/frontend/planilla/PlanillaComponent.vue").default
);
Vue.component(
    "dashboard-compras-order-component",
    require("./components/dashboard/compras/order/OrderComponent.vue").default
);
Vue.component(
    "dashboard-compras-inscripcion-component",
    require("./components/dashboard/compras/inscripcion/OrderComponent.vue").default
);
Vue.component(
    "dashboard-compras-datatable-component",
    require("./components/dashboard/compras/datatables/DatatableComponent.vue")
        .default
);
Vue.component(
    "dashboard-config-bancos-datatable-component",
    require("./components/dashboard/config/bancos/DatatableComponent.vue")
        .default
);
Vue.component(
    "dashboard-config-promo-code-datatable-component",
    require("./components/dashboard/config/PromoCode/DatatableComponent.vue")
        .default
);
Vue.component(
    "dashboard-pagos-verification-form-component",
    require("./components/dashboard/pagos/TransferComponent.vue").default
);
Vue.component(
    "dashboard-pagos-verification-bancos-component",
    require("./components/dashboard/pagos/BancosComponent.vue").default
);
Vue.component(
    "dashboard-pagos-confirmation-info-component",
    require("./components/dashboard/compras/confirmation/InfoComponent.vue")
        .default
);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
Vue.filter("formatDate", function(value) {
    if (value) {
        var date = new Date(value);

        return (
            date.getDate() +
            "/" +
            (date.getMonth() + 1) +
            "/" +
            date.getFullYear()
        );
    }
});

const app = new Vue({
    el: "#containerbar",
    created(){
        this.axiosConfig();
    },
    mounted(){
        this.verifyShopCar();
        
    },
    data(){
        return {
            elementInCar:null,
            auth_token: null,
            dashboard_url: "/dashboard"
        }
    },
    methods: {
        verifyShopCar(){
            let c = localStorage.getItem('car')
            if(c){
               c = JSON.parse(c)
            }else{
                return false
            }
            if(c.length > 0){
                this.elementInCar = c.length
            }else{
                this.elementInCar = null
            }
        },
        axiosConfig() {
            var output = {};
            document.cookie.split(/\s*;\s*/).forEach(pair => {
                pair = pair.split(/\s*=\s*/);
                output[pair[0]] = pair.splice(1).join("=");
            });

            this.auth_token = output.auth_token;
            console.log(output.auth_token);
            if (!this.auth_token) {
                this.logout()
            }
            localStorage.setItem("req_token", output.auth_token);
            axios.defaults.headers.common["Authorization"] = output.auth_token
                ? "Bearer " +
                  decodeURIComponent(localStorage.getItem("req_token"))
                : "";
        },
        openModal() {
            let path = location.pathname;
            if (path.includes(this.dashboard_url)) {
                $("#defaultModal").modal("show");
                setTimeout(() => {
                    this.closeModal();
                }, 300000);
            }
        },
        logout() {
            let path = location.pathname;
            if (path.includes(this.dashboard_url)) {
                localStorage.removeItem('req_token')
                document.getElementById("logout-form").submit();
            }
            
        },

    }
});
