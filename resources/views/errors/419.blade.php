@extends('layouts.errors.app',['title' => '419'])

@section('title', __('Page Expired'))
@section('code', '419')
@section('message', __('Page Expired'))
