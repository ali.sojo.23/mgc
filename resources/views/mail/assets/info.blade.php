@if ($status['code'] == 1)
<div style="background-color: transparent;">
    <div
        class="block-grid"
        style="
            margin: 0 auto;
            min-width: 320px;
            max-width: 650px;
            overflow-wrap: break-word;
            word-wrap: break-word;
            word-break: break-word;
            background-color: #f0f0f0;
        "
    >
        <div
            style="
                border-collapse: collapse;
                display: table;
                width: 100%;
                background-color: #f0f0f0;
            "
        >
            <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:transparent;"><tr><td align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:650px"><tr class="layout-full-width" style="background-color:#F0F0F0"><![endif]-->
            <!--[if (mso)|(IE)]><td align="center" width="650" style="background-color:#F0F0F0;width:650px; border-top: none; border-left: none; border-bottom: none; border-right: none;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr bgcolor='#FFFFFF'><td colspan='3' style='font-size:7px;line-height:18px'>&nbsp;</td></tr><tr><td style='padding-top:15px;padding-bottom:15px' width='25' bgcolor='#FFFFFF'><table role='presentation' width='25' cellpadding='0' cellspacing='0' border='0'><tr><td>&nbsp;</td></tr></table></td><td style="padding-right: 35px; padding-left: 35px; padding-top:15px; padding-bottom:5px;"><![endif]-->
            <div
                class="col num12"
                style="
                    min-width: 320px;
                    max-width: 650px;
                    display: table-cell;
                    vertical-align: top;
                    width: 600px;
                "
            >
                <div style="width: 100% !important;">
                    <!--[if (!mso)&(!IE)]><!-->
                    <div
                        style="
                            border-top: 18px solid
                                #ffffff;
                            border-left: 25px solid
                                #ffffff;
                            border-bottom: 18px solid
                                #ffffff;
                            border-right: 25px solid
                                #ffffff;
                            padding-top: 15px;
                            padding-bottom: 5px;
                            padding-right: 35px;
                            padding-left: 35px;
                        "
                    >
                        <!--<![endif]-->
                        <!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 15px; padding-left: 15px; padding-top: 15px; padding-bottom: 10px; font-family: Tahoma, Verdana, sans-serif"><![endif]-->
                        <div
                            style="
                                color: #052d3d;
                                font-family: Lato,
                                    Tahoma, Verdana,
                                    Segoe, sans-serif;
                                line-height: 1.2;
                                padding-top: 15px;
                                padding-right: 15px;
                                padding-bottom: 10px;
                                padding-left: 15px;
                            "
                        >
                            <div
                                style="
                                    line-height: 1.2;
                                    font-size: 12px;
                                    font-family: Lato,
                                        Tahoma, Verdana,
                                        Segoe,
                                        sans-serif;
                                    color: #052d3d;
                                    mso-line-height-alt: 14px;
                                "
                            >
                                <p
                                    style="
                                        line-height: 1.2;
                                        font-size: 34px;
                                        text-align: center;
                                        font-family: Lato,
                                            Tahoma,
                                            Verdana,
                                            Segoe,
                                            sans-serif;
                                        word-break: break-word;
                                        mso-line-height-alt: 41px;
                                        margin: 0;
                                    "
                                >
                                    <span
                                        style="
                                            font-size: 34px;
                                        "
                                        ><span
                                            style="
                                                color: red;
                                                font-size: 34px;
                                            "
                                            ><strong
                                                ><span
                                                    style="
                                                        font-size: 34px;
                                                    "
                                                    >Hemos registrado el pago de su inscripción
                                                    <br /></span></strong></span
                                        >
                                </p>
                            </div>
                        </div>
                        <!--[if mso]></td></tr></table><![endif]-->
                        <!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px; padding-left: 10px; padding-top: 0px; padding-bottom: 30px; font-family: Tahoma, Verdana, sans-serif"><![endif]-->
                        <div
                            style="
                                color: #787878;
                                font-family: Lato,
                                    Tahoma, Verdana,
                                    Segoe, sans-serif;
                                line-height: 1.5;
                                padding-top: 0px;
                                padding-right: 10px;
                                padding-bottom: 30px;
                                padding-left: 10px;
                            "
                        >
                            <div
                                style="
                                    font-size: 12px;
                                    line-height: 1.5;
                                    font-family: Lato,
                                        Tahoma, Verdana,
                                        Segoe,
                                        sans-serif;
                                    color: #787878;
                                    mso-line-height-alt: 18px;
                                "
                            >
                                <p
                                    style="
                                        font-size: 18px;
                                        line-height: 1.5;
                                        text-align: center;
                                        font-family: Lato,
                                            Tahoma,
                                            Verdana,
                                            Segoe,
                                            sans-serif;
                                        word-break: break-word;
                                        mso-line-height-alt: 27px;
                                        margin: 0;
                                    "
                                >
                                    <span
                                        style="
                                            font-size: 18px;
                                        "
                                        >Hemos registrado el pago de su planilla de inscripción #{{$order->id}}, el mismo se encuentra en verificación, cuando el mismo sea aprobado le será notificado vía correo electrónico. <br>
                                        en caso de no recibir el correo de notificación puede verificar el estado del mismo en el area de clientes, o puede contactarnos por el siguiente correo electrónico
                                        <strong
                                            ><a
                                                href="mailto:support@megalcaacademy.com"
                                                rel="noopener"
                                                style="
                                                    text-decoration: none;
                                                    color: #2190e3;
                                                "
                                                target="_blank"
                                                >support@megalcaacademy.com</a
                                            ></strong
                                        >
                                        o a travéz del whatsapp haciendo
                                        <strong
                                            ><a
                        href="https://wa.me/584164319133?text=Buenas%20tardes%20estoy%20teniendo%20problemas%20con%20la%20confirmación%20del%20pago%20de%20la%20planilla%20de%20inscripción%20número%20{{$order->id}}"
                                                rel="noopener"
                                                style="
                                                    text-decoration: none;
                                                    color: #2190e3;
                                                "
                                                target="_blank"
                                                >click aquí</a
                                            ></strong
                                        >
                                        </span
                                    >
                                </p>
                            </div>
                        </div>
                        <!--[if mso]></td></tr></table><![endif]-->
                        <!--[if (!mso)&(!IE)]><!-->
                    </div>
                    <!--<![endif]-->
                </div>
            </div>
            <!--[if (mso)|(IE)]></td><td style='padding-top:15px;padding-bottom:15px' width='25' bgcolor='#FFFFFF'><table role='presentation' width='25' cellpadding='0' cellspacing='0' border='0'><tr><td>&nbsp;</td></tr></table></td></tr><tr bgcolor='#FFFFFF'><td colspan='3' style='font-size:7px;line-height:18px'>&nbsp;</td></tr></table><![endif]-->
            <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
        </div>
    </div>
</div> 
@elseif($status['code'] == 2)
<div style="background-color: transparent;">
    <div
        class="block-grid"
        style="
            margin: 0 auto;
            min-width: 320px;
            max-width: 650px;
            overflow-wrap: break-word;
            word-wrap: break-word;
            word-break: break-word;
            background-color: #f0f0f0;
        "
    >
        <div
            style="
                border-collapse: collapse;
                display: table;
                width: 100%;
                background-color: #f0f0f0;
            "
        >
            <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:transparent;"><tr><td align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:650px"><tr class="layout-full-width" style="background-color:#F0F0F0"><![endif]-->
            <!--[if (mso)|(IE)]><td align="center" width="650" style="background-color:#F0F0F0;width:650px; border-top: none; border-left: none; border-bottom: none; border-right: none;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr bgcolor='#FFFFFF'><td colspan='3' style='font-size:7px;line-height:18px'>&nbsp;</td></tr><tr><td style='padding-top:15px;padding-bottom:15px' width='25' bgcolor='#FFFFFF'><table role='presentation' width='25' cellpadding='0' cellspacing='0' border='0'><tr><td>&nbsp;</td></tr></table></td><td style="padding-right: 35px; padding-left: 35px; padding-top:15px; padding-bottom:5px;"><![endif]-->
            <div
                class="col num12"
                style="
                    min-width: 320px;
                    max-width: 650px;
                    display: table-cell;
                    vertical-align: top;
                    width: 600px;
                "
            >
                <div style="width: 100% !important;">
                    <!--[if (!mso)&(!IE)]><!-->
                    <div
                        style="
                            border-top: 18px solid
                                #ffffff;
                            border-left: 25px solid
                                #ffffff;
                            border-bottom: 18px solid
                                #ffffff;
                            border-right: 25px solid
                                #ffffff;
                            padding-top: 15px;
                            padding-bottom: 5px;
                            padding-right: 35px;
                            padding-left: 35px;
                        "
                    >
                        <!--<![endif]-->
                        <!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 15px; padding-left: 15px; padding-top: 15px; padding-bottom: 10px; font-family: Tahoma, Verdana, sans-serif"><![endif]-->
                        <div
                            style="
                                color: #052d3d;
                                font-family: Lato,
                                    Tahoma, Verdana,
                                    Segoe, sans-serif;
                                line-height: 1.2;
                                padding-top: 15px;
                                padding-right: 15px;
                                padding-bottom: 10px;
                                padding-left: 15px;
                            "
                        >
                            <div
                                style="
                                    line-height: 1.2;
                                    font-size: 12px;
                                    font-family: Lato,
                                        Tahoma, Verdana,
                                        Segoe,
                                        sans-serif;
                                    color: #052d3d;
                                    mso-line-height-alt: 14px;
                                "
                            >
                                <p
                                    style="
                                        line-height: 1.2;
                                        font-size: 34px;
                                        text-align: center;
                                        font-family: Lato,
                                            Tahoma,
                                            Verdana,
                                            Segoe,
                                            sans-serif;
                                        word-break: break-word;
                                        mso-line-height-alt: 41px;
                                        margin: 0;
                                    "
                                >
                                    <span
                                        style="
                                            font-size: 34px;
                                        "
                                        ><span
                                            style="
                                                color: red;
                                                font-size: 34px;
                                            "
                                            ><strong
                                                ><span
                                                    style="
                                                        font-size: 34px;
                                                    "
                                                    >Hemos procesado el pago de su inscripción
                                                    <br /></span></strong></span
                                        >
                                </p>
                            </div>
                        </div>
                        <!--[if mso]></td></tr></table><![endif]-->
                        <!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px; padding-left: 10px; padding-top: 0px; padding-bottom: 30px; font-family: Tahoma, Verdana, sans-serif"><![endif]-->
                        <div
                            style="
                                color: #787878;
                                font-family: Lato,
                                    Tahoma, Verdana,
                                    Segoe, sans-serif;
                                line-height: 1.5;
                                padding-top: 0px;
                                padding-right: 10px;
                                padding-bottom: 30px;
                                padding-left: 10px;
                            "
                        >
                            <div
                                style="
                                    font-size: 12px;
                                    line-height: 1.5;
                                    font-family: Lato,
                                        Tahoma, Verdana,
                                        Segoe,
                                        sans-serif;
                                    color: #787878;
                                    mso-line-height-alt: 18px;
                                "
                            >
                                <p
                                    style="
                                        font-size: 18px;
                                        line-height: 1.5;
                                        text-align: center;
                                        font-family: Lato,
                                            Tahoma,
                                            Verdana,
                                            Segoe,
                                            sans-serif;
                                        word-break: break-word;
                                        mso-line-height-alt: 27px;
                                        margin: 0;
                                    "
                                >
                                    <span
                                        style="
                                            font-size: 18px;
                                        "
                                        >Hemos procesado el pago de su planilla de inscripción #{{$order->id}}, dentro de poco estará recibiendo el acceso a nuestro portal de estudios. <br>
                                        en caso de no recibir el correo con los accesos puede contactarnos por el siguiente correo electrónico
                                        <strong
                                            ><a
                                                href="mailto:support@megalcaacademy.com"
                                                rel="noopener"
                                                style="
                                                    text-decoration: none;
                                                    color: #2190e3;
                                                "
                                                target="_blank"
                                                >support@megalcaacademy.com</a
                                            ></strong
                                        >
                                        o a travéz del whatsapp haciendo
                                        <strong
                                            ><a
                        href="https://wa.me/584164319133?text=Buenas%20tardes%20estoy%20teniendo%20problemas%20con%20la%20confirmación%20del%20pago%20de%20la%20planilla%20de%20inscripción%20número%20{{$order->id}}"
                                                rel="noopener"
                                                style="
                                                    text-decoration: none;
                                                    color: #2190e3;
                                                "
                                                target="_blank"
                                                >click aquí</a
                                            ></strong
                                        >
                                        </span
                                    >
                                </p>
                            </div>
                        </div>
                        <!--[if mso]></td></tr></table><![endif]-->
                        <!--[if (!mso)&(!IE)]><!-->
                    </div>
                    <!--<![endif]-->
                </div>
            </div>
            <!--[if (mso)|(IE)]></td><td style='padding-top:15px;padding-bottom:15px' width='25' bgcolor='#FFFFFF'><table role='presentation' width='25' cellpadding='0' cellspacing='0' border='0'><tr><td>&nbsp;</td></tr></table></td></tr><tr bgcolor='#FFFFFF'><td colspan='3' style='font-size:7px;line-height:18px'>&nbsp;</td></tr></table><![endif]-->
            <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
        </div>
    </div>
</div> 
@elseif($status['code'] == 3)
<div style="background-color: transparent;">
    <div
        class="block-grid"
        style="
            margin: 0 auto;
            min-width: 320px;
            max-width: 650px;
            overflow-wrap: break-word;
            word-wrap: break-word;
            word-break: break-word;
            background-color: #f0f0f0;
        "
    >
        <div
            style="
                border-collapse: collapse;
                display: table;
                width: 100%;
                background-color: #f0f0f0;
            "
        >
            <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:transparent;"><tr><td align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:650px"><tr class="layout-full-width" style="background-color:#F0F0F0"><![endif]-->
            <!--[if (mso)|(IE)]><td align="center" width="650" style="background-color:#F0F0F0;width:650px; border-top: none; border-left: none; border-bottom: none; border-right: none;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr bgcolor='#FFFFFF'><td colspan='3' style='font-size:7px;line-height:18px'>&nbsp;</td></tr><tr><td style='padding-top:15px;padding-bottom:15px' width='25' bgcolor='#FFFFFF'><table role='presentation' width='25' cellpadding='0' cellspacing='0' border='0'><tr><td>&nbsp;</td></tr></table></td><td style="padding-right: 35px; padding-left: 35px; padding-top:15px; padding-bottom:5px;"><![endif]-->
            <div
                class="col num12"
                style="
                    min-width: 320px;
                    max-width: 650px;
                    display: table-cell;
                    vertical-align: top;
                    width: 600px;
                "
            >
                <div style="width: 100% !important;">
                    <!--[if (!mso)&(!IE)]><!-->
                    <div
                        style="
                            border-top: 18px solid
                                #ffffff;
                            border-left: 25px solid
                                #ffffff;
                            border-bottom: 18px solid
                                #ffffff;
                            border-right: 25px solid
                                #ffffff;
                            padding-top: 15px;
                            padding-bottom: 5px;
                            padding-right: 35px;
                            padding-left: 35px;
                        "
                    >
                        <!--<![endif]-->
                        <!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 15px; padding-left: 15px; padding-top: 15px; padding-bottom: 10px; font-family: Tahoma, Verdana, sans-serif"><![endif]-->
                        <div
                            style="
                                color: #052d3d;
                                font-family: Lato,
                                    Tahoma, Verdana,
                                    Segoe, sans-serif;
                                line-height: 1.2;
                                padding-top: 15px;
                                padding-right: 15px;
                                padding-bottom: 10px;
                                padding-left: 15px;
                            "
                        >
                            <div
                                style="
                                    line-height: 1.2;
                                    font-size: 12px;
                                    font-family: Lato,
                                        Tahoma, Verdana,
                                        Segoe,
                                        sans-serif;
                                    color: #052d3d;
                                    mso-line-height-alt: 14px;
                                "
                            >
                                <p
                                    style="
                                        line-height: 1.2;
                                        font-size: 34px;
                                        text-align: center;
                                        font-family: Lato,
                                            Tahoma,
                                            Verdana,
                                            Segoe,
                                            sans-serif;
                                        word-break: break-word;
                                        mso-line-height-alt: 41px;
                                        margin: 0;
                                    "
                                >
                                    <span
                                        style="
                                            font-size: 34px;
                                        "
                                        ><span
                                            style="
                                                color: green;
                                                font-size: 34px;
                                            "
                                            ><strong
                                                ><span
                                                    style="
                                                        font-size: 34px;
                                                    "
                                                    >Hemos procesado su inscripción
                                                    <br /></span></strong></span
                                        >
                                </p>
                            </div>
                        </div>
                        <!--[if mso]></td></tr></table><![endif]-->
                        <!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px; padding-left: 10px; padding-top: 0px; padding-bottom: 30px; font-family: Tahoma, Verdana, sans-serif"><![endif]-->
                        <div
                            style="
                                color: #787878;
                                font-family: Lato,
                                    Tahoma, Verdana,
                                    Segoe, sans-serif;
                                line-height: 1.5;
                                padding-top: 0px;
                                padding-right: 10px;
                                padding-bottom: 30px;
                                padding-left: 10px;
                            "
                        >
                            <div
                                style="
                                    font-size: 12px;
                                    line-height: 1.5;
                                    font-family: Lato,
                                        Tahoma, Verdana,
                                        Segoe,
                                        sans-serif;
                                    color: #787878;
                                    mso-line-height-alt: 18px;
                                "
                            >
                                <p
                                    style="
                                        font-size: 18px;
                                        line-height: 1.5;
                                        text-align: center;
                                        font-family: Lato,
                                            Tahoma,
                                            Verdana,
                                            Segoe,
                                            sans-serif;
                                        word-break: break-word;
                                        mso-line-height-alt: 27px;
                                        margin: 0;
                                    "
                                >
                                    <span
                                        style="
                                            font-size: 18px;
                                        "
                                        >Hemos procesado su inscripción #{{$order->id}}, dentro de poco estará recibiendo el acceso a nuestro portal de estudios. <br>
                                        en caso de no recibir el correo con los accesos puede contactarnos por el siguiente correo electrónico
                                        <strong
                                            ><a
                                                href="mailto:support@megalcaacademy.com"
                                                rel="noopener"
                                                style="
                                                    text-decoration: none;
                                                    color: #2190e3;
                                                "
                                                target="_blank"
                                                >support@megalcaacademy.com</a
                                            ></strong
                                        >
                                        o a travéz del whatsapp haciendo
                                        <strong
                                            ><a
                        href="https://wa.me/584164319133?text=Buenas%20tardes%20estoy%20teniendo%20problemas%20con%20la%20confirmación%20del%20pago%20de%20la%20planilla%20de%20inscripción%20número%20{{$order->id}}"
                                                rel="noopener"
                                                style="
                                                    text-decoration: none;
                                                    color: #2190e3;
                                                "
                                                target="_blank"
                                                >click aquí</a
                                            ></strong
                                        >
                                        </span
                                    >
                                </p>
                            </div>
                        </div>
                        <!--[if mso]></td></tr></table><![endif]-->
                        <!--[if (!mso)&(!IE)]><!-->
                    </div>
                    <!--<![endif]-->
                </div>
            </div>
            <!--[if (mso)|(IE)]></td><td style='padding-top:15px;padding-bottom:15px' width='25' bgcolor='#FFFFFF'><table role='presentation' width='25' cellpadding='0' cellspacing='0' border='0'><tr><td>&nbsp;</td></tr></table></td></tr><tr bgcolor='#FFFFFF'><td colspan='3' style='font-size:7px;line-height:18px'>&nbsp;</td></tr></table><![endif]-->
            <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
        </div>
    </div>
</div> 
@endif

<div style="background-color: transparent;">
    <div
        class="block-grid"
        style="
            margin: 0 auto;
            min-width: 320px;
            max-width: 650px;
            overflow-wrap: break-word;
            word-wrap: break-word;
            word-break: break-word;
            background-color: #f0f0f0;
        "
    >
        <div
            style="
                border-collapse: collapse;
                display: table;
                width: 100%;
                background-color: #f0f0f0;
            "
        >
            <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:transparent;"><tr><td align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:650px"><tr class="layout-full-width" style="background-color:#F0F0F0"><![endif]-->
            <!--[if (mso)|(IE)]><td align="center" width="650" style="background-color:#F0F0F0;width:650px; border-top: none; border-left: none; border-bottom: none; border-right: none;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr bgcolor='#FFFFFF'><td colspan='3' style='font-size:7px;line-height:18px'>&nbsp;</td></tr><tr><td style='padding-top:15px;padding-bottom:15px' width='25' bgcolor='#FFFFFF'><table role='presentation' width='25' cellpadding='0' cellspacing='0' border='0'><tr><td>&nbsp;</td></tr></table></td><td style="padding-right: 35px; padding-left: 35px; padding-top:15px; padding-bottom:5px;"><![endif]-->
            <div
                class="col num12"
                style="
                    min-width: 320px;
                    max-width: 650px;
                    display: table-cell;
                    vertical-align: top;
                    width: 600px;
                "
            >
                <div style="width: 100% !important;">
                    <!--[if (!mso)&(!IE)]><!-->
                    <div
                        style="
                            border-top: 18px solid
                                #ffffff;
                            border-left: 25px solid
                                #ffffff;
                            border-bottom: 18px solid
                                #ffffff;
                            border-right: 25px solid
                                #ffffff;
                            padding-top: 15px;
                            padding-bottom: 5px;
                            padding-right: 35px;
                            padding-left: 35px;
                        "
                    >
                        <!--<![endif]-->
                        <!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 15px; padding-left: 15px; padding-top: 15px; padding-bottom: 10px; font-family: Tahoma, Verdana, sans-serif"><![endif]-->
                        <div
                            style="
                                color: #052d3d;
                                font-family: Lato,
                                    Tahoma, Verdana,
                                    Segoe, sans-serif;
                                line-height: 1.2;
                                padding-top: 15px;
                                padding-right: 15px;
                                padding-bottom: 10px;
                                padding-left: 15px;
                            "
                        >
                            <div
                                style="
                                    line-height: 1.2;
                                    font-size: 12px;
                                    font-family: Lato,
                                        Tahoma, Verdana,
                                        Segoe,
                                        sans-serif;
                                    color: #052d3d;
                                    mso-line-height-alt: 14px;
                                "
                            >
                                <p
                                    style="
                                        line-height: 1.2;
                                        font-size: 34px;
                                        text-align: center;
                                        font-family: Lato,
                                            Tahoma,
                                            Verdana,
                                            Segoe,
                                            sans-serif;
                                        word-break: break-word;
                                        mso-line-height-alt: 41px;
                                        margin: 0;
                                    "
                                >
                                    <span
                                        style="
                                            font-size: 34px;
                                        "
                                        ><span
                                            style="
                                                color: #fc7318;
                                                font-size: 34px;
                                            "
                                            ><strong
                                                ><span
                                                    style="
                                                        font-size: 34px;
                                                    "
                                                    >¿Recibiste el correo de manera equivocada?
                                                    <br /></span></strong></span
                                        >
                                </p>
                            </div>
                        </div>
                        <!--[if mso]></td></tr></table><![endif]-->
                        <!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px; padding-left: 10px; padding-top: 0px; padding-bottom: 30px; font-family: Tahoma, Verdana, sans-serif"><![endif]-->
                        <div
                            style="
                                color: #787878;
                                font-family: Lato,
                                    Tahoma, Verdana,
                                    Segoe, sans-serif;
                                line-height: 1.5;
                                padding-top: 0px;
                                padding-right: 10px;
                                padding-bottom: 30px;
                                padding-left: 10px;
                            "
                        >
                            <div
                                style="
                                    font-size: 12px;
                                    line-height: 1.5;
                                    font-family: Lato,
                                        Tahoma, Verdana,
                                        Segoe,
                                        sans-serif;
                                    color: #787878;
                                    mso-line-height-alt: 18px;
                                "
                            >
                                <p
                                    style="
                                        font-size: 18px;
                                        line-height: 1.5;
                                        text-align: center;
                                        font-family: Lato,
                                            Tahoma,
                                            Verdana,
                                            Segoe,
                                            sans-serif;
                                        word-break: break-word;
                                        mso-line-height-alt: 27px;
                                        margin: 0;
                                    "
                                >
                                    <span
                                        style="
                                            font-size: 18px;
                                        "
                                        >puede hacer caso omiso a este mensaje. o enviar un correo de notificación a:
                                        <strong
                                            ><a
                                                href="mailto:support@megalcaacademy.com"
                                                rel="noopener"
                                                style="
                                                    text-decoration: none;
                                                    color: #2190e3;
                                                "
                                                target="_blank"
                                                >support@megalcaacademy.com</a
                                            ></strong
                                        ></span
                                    >
                                </p>
                            </div>
                        </div>
                        <!--[if mso]></td></tr></table><![endif]-->
                        <!--[if (!mso)&(!IE)]><!-->
                    </div>
                    <!--<![endif]-->
                </div>
            </div>
            <!--[if (mso)|(IE)]></td><td style='padding-top:15px;padding-bottom:15px' width='25' bgcolor='#FFFFFF'><table role='presentation' width='25' cellpadding='0' cellspacing='0' border='0'><tr><td>&nbsp;</td></tr></table></td></tr><tr bgcolor='#FFFFFF'><td colspan='3' style='font-size:7px;line-height:18px'>&nbsp;</td></tr></table><![endif]-->
            <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
        </div>
    </div>
</div>