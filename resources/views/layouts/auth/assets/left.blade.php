<div class="col-md-6 col-lg-5">
    <div class="auth-box-left">
        <div class="card">
            <div class="card-body text-center">
                
                <div class="auth-box-icon">
                    <img src="{{asset('assets/images/authentication/auth-box-icon.svg')}}" class="img-fluid" alt="auth-box-icon">
                </div>
                <div class="auth-box-logo">
                    <img src="{{asset('assets/images/logo.svg')}}"  height="70px" alt="logo">
                </div>
            </div>
        </div>
    </div>
</div>